/**
 * Viene Bien Aplicación
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useContext, useEffect } from 'react';

import { NavigationContainer,  } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen, OnboardingScreen, 
  PlatoScreen, SearchScreeen, ListaScreen } from './app/routes'
import { ListaProvider, PlatosProvider, PlatosContext} from './app/context'
import HandleNotifications from './app/api/NotificationHandler'
import { getData, storeData } from './app/api/saveToLocal'

import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import Store from "./app/context/MenuStore"
import { Provider } from "mobx-react";

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator();

function TabStack() {
  const { pantalla,  setPantalla } = useContext(PlatosContext)

  return (
    <Tab.Navigator
        initialRouteName= {pantalla}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color, size }) => {
            let iconName; 

            if (route.name === 'Descubrir') {
              iconName = 'search';
            } else if (route.name === 'Lista de compras') {
              iconName = 'shopping-cart';
            }
            // You can return any component that you like here!
            return <FontAwesome5 
              name={iconName} 
              size={size} 
              color={color}
            />
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
          labelStyle: {
            fontSize: 12 ,
            margin: 0
          },
          style: {
            height: 55,
            paddingTop: 5,
            paddingBottom: 5,
          },
        }}
      >
        <Tab.Screen name="Descubrir" component={HomeScreen} />
        <Tab.Screen name="Lista de compras" component={ListaScreen} />
      </Tab.Navigator>
  )
}

function MainStack() {
  return (
    <Stack.Navigator
      initialRouteName= "Onboarding"
      headerMode = 'screen'
      mode='modal'
      cardStyle = {{ backgroundColor: '#FFFFFF' }}
    >
        <Stack.Screen 
          name="Onboarding" 
          component={OnboardingScreen}
          options= {{headerShown: false}} 
        />

        <Stack.Screen 
          name="Home" 
          component={TabStack} 
          options= {{headerShown: false}} 
        />

        <Stack.Screen 
          name="Plato"
          component={PlatoScreen} 
          options={() => {
            return {
              headerShown: false,
          }}} />
        
        <Stack.Screen 
          name="Search"
          component={SearchScreeen} 
          options= {{headerShown: false}} 
        />
      </Stack.Navigator>
  );
}

const Auth = new HandleNotifications()
function App() {  
  useEffect(() => {
    getData("alreadyLaunched").then(value => {
      if(value!==true){
          storeData('alreadyLaunched', true); // No need to wait for `setItem` to finish, although you might want to handle errors
          Auth.registerNewUser()
      }
      else Auth.checkUpdate()
    })
    Auth.isFromNotification()
  }, [])

  return (
      <Provider store={Store}>
      <ListaProvider>
        <PlatosProvider>
        <NavigationContainer><MainStack/></NavigationContainer>
        </PlatosProvider>
      </ListaProvider>
      </Provider>
  );
}

export default App;