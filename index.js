/**
 * This function adds one to its input.
 * @param {number} input any number
 * @returns {number} that number, plus one.
 */
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import 'moment/locale/es';  // language must match config
import RNUxcam from 'react-native-ux-cam';
//RNUxcam.optIntoSchematicRecordings(); // Add this line to enable iOS screen recordings
//RNUxcam.startWithKey('259bxreaekm1gge'); // Add this line after

AppRegistry.registerComponent(appName, () => App);