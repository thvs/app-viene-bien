import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  card: {
    flex: 1,
    flexGrow: 1, 
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -15,
    paddingTop: 0,
    paddingBottom: -10
  }
})