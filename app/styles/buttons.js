import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    color : '#fff'
  },
  primary: {
    margin: 15,
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width: 1, height: 10},
    shadowRadius: 20,
    borderRadius: 10,
    color: '#fff'
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: '#fff'
  },
  goback: {
    left: 0,
  },
});
