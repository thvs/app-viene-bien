import { Share } from 'react-native';

const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          `Viene Bien te brinda la posibilidad de armar el menú semanal para toda tu familia considerando recetas personalizadas para cada miembro de tu familia. Pruebala aqui https://play.google.com/store/apps/details?id=com.appvienebien`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
};

export default function() {
  onShare().then(console.log())
};
