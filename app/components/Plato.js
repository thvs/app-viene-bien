import React from 'react';
import { View, StyleSheet, Dimensions, 
    TouchableOpacity, Image, Text } from 'react-native';


const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.39);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH / 0.8);
    
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

/**
 * Component para mostrar plato individual info corta[]
 *
 * @component
 * @example
 * const age = 21
 * const name = 'Jitendra Nirnejak'
 * return (
 *   <User age={age} name={name} />
 * )
 */
/**
 * @param  {} {nombre}
 * @param  {} {precio}
 * @param  {} {uri}
 * @param  {} {tiempo}
 * @param  {} {onPress}
 */
export default ({nombre, precio, uri, tiempo, onPress}) => {
    return <>
    <TouchableOpacity onPress={onPress}>
        <Image
        source={{ uri: uri}}
        style={{
            width: ITEM_WIDTH,
            height: ITEM_HEIGHT,
            borderRadius:14,
            backgroundColor: '#eee'
        }}/>
        <Text style={styles.tiempo}>
          <FontAwesome5 name="clock" size={14} />
            {`  ${tiempo}`}
        </Text>
    </TouchableOpacity>
    <View style={{flexWrap: 'wrap'}}>
        <Text style={styles.itemLabel}>
            {nombre}
        </Text>
        <Text style={{ color: '#FF8A00', paddingTop: 5 }}>
          {`S/${precio}`}
          <Text style={{ color: '#bbb' }}>  x </Text>  
          <Icon name="user" size={14} color="#bbb"/>
        </Text>
    </View>
  </>
};

const styles = StyleSheet.create({
  container: { flex: 1 },
  itemLabel: {
    color: 'black',
    fontSize: 16,
    alignSelf:'center',
    paddingTop: 4,
    fontFamily:'BRFirma-Bold'
  },
  tiempo:{
    color: 'white',
    position: 'absolute', // child
    bottom: 13, // position where you want
    right: 0,
    fontSize: 13,
    padding: 6,
    paddingTop: 4,
    paddingBottom: 4,
    borderRadius:5,
    backgroundColor: '#FF8A00',
    fontFamily: 'BRFirma-Regular'
  }
});
