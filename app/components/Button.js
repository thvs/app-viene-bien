import React from 'react'

import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'


export default ({ text, type, onPress, children, extraStyle = {}, extraText = {} }) => {
  return (
      <>
        {type==='primary' && 
        <TouchableOpacity
          style={{...styles.button, ...extraStyle}}
          accessibilityLabel={text}
          onPress={onPress?onPress:() => console.log('Pressed')}
        >
          <Text style={{...styles.buttonText, ...extraText}}>
            {text}
          </Text>
        </TouchableOpacity>
        }
        {type==='child' && 
        <TouchableOpacity
          style={{...styles.button, ...extraStyle}}
          accessibilityLabel={text}
          onPress={onPress?onPress:() => console.log('Pressed')}
        >
          {children}
        </TouchableOpacity>
        }
        {type==='secondary' && 
        <View>
          <TouchableOpacity
            style={{
              backgroundColor: '#FF8A00', 
              padding: 10,
              paddingVertical: 8,
              borderRadius: 12,
            }}
            onPress={onPress?onPress:() => console.log('Pressed')}>
              <Text style={{fontSize: 14, color:"#fff", fontFamily: 'BRFirma-Regular'}}>
              {text}
              </Text>
          </TouchableOpacity>
        </View>}
      </>
    )
}


const styles = StyleSheet.create({
  button:{
    backgroundColor: '#FF8A00',
    padding: 10,   
    paddingHorizontal: 15,
    alignItems: "center",
    borderRadius: 12, 
    justifyContent: 'flex-end',
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width: 1, height: 10},
    shadowRadius: 20,
    borderRadius: 10,
    width: '80%',
    elevation: 5,
    alignSelf: 'center',
  },
  buttonText:{
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontFamily:'BRFirma-Bold'
  }
});