import * as React from 'react';
import { Searchbar } from 'react-native-paper';


export default ({placeholder, crrQuery = '', setSearchQuery, queryAction = ()=>{}}) => {
  const onChangeSearch = query => setSearchQuery(query);

  return (
    <Searchbar
      placeholder={placeholder}
      onChangeText={onChangeSearch}
      onPress={queryAction}
      value={crrQuery}
      style={{
        fontSize: 14,
        borderRadius: 12,
        marginLeft: 15,
        marginRight: 15,
        fontFamily: 'BRFirma-Regular'
      }}
      onSubmitEditing={()=> queryAction() } // <== Your Navigation handler
      returnKeyType='search'
      iconColor="#333"
    />
  );
};