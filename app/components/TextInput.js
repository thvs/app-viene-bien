import * as React from 'react';
import { TextInput } from 'react-native-paper';

export default ({type, placeholder}) => {
  const [text, setText] = React.useState('');

  return (
    <TextInput
        style={{ backgroundColor: '#FFFFFF'}}
        label={type}
        placeholder={placeholder}
        value={text}
        onChangeText={text => setText(text)}
        left={<TextInput.Icon name="select-search"/>}
    />
  );
};
