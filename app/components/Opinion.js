import React, { useState } from 'react'
import {View, Text, Image, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import { Button } from './index'

import Modal from 'react-native-modal';
import analytics from '@react-native-firebase/analytics';
import firestore from '@react-native-firebase/firestore';

/**
 * 4 Modals / Begining, two questions... terminos?????????????, last 
 */

export default ({closeModal}) => {
  const [crrModal, setModal] = useState(()=>0)
  const [opinion, setOpinion] = React.useState('');
  const [telefono, setTelefono] = React.useState('');

  const enviarInfo = () => {    
    analytics()
    .logEvent('boton_participar', {
      telefono: telefono
    })

    firestore()
    .collection("telefonos").add({
        number: telefono,
        date: new Date()
    })
    .then(function(docRef) {
    console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
  }


  return (
    <>
      <Modal isVisible={crrModal===0}>
        <View style={styles.container}>
          <Image 
            source={require('../assets/img/canasta.png')}
            style={{
              width: 107,
              height: 120,
              marginBottom: 10,
            }}/>
            <Text style={{textAlign: 'center', ...styles.title}}>
              ¿Quisieras ganar una canasta llena de productos GRATIS?
            </Text>
            <Button 
              text={'Sí, sí quiero.'} 
              type='primary' 
              onPress={()=> {setModal(1);     
                analytics().logEvent('boton_si_quiero_participar')
            }}/> 
            <TouchableOpacity onPress={()=>closeModal(false)}>
              <Text style={{fontFamily: 'BRFirma-Bold', marginTop: 5, fontSize: 14}}>No, no me interesa</Text>
              </TouchableOpacity>
            </View>
        </Modal>
      <Modal isVisible={crrModal===-1}>
        <View
        style={{
            backgroundColor: 'white',
            padding: 20,
            borderRadius: 12,
        }}
        >
        <Text style={styles.title}>
          Déjanos una pequeña opinión y participa del sorteo</Text>
        <TextInput
                type='outlined'
                multiline
                label="¿Qué te gustaría mejorar en la app?"
                placeholder='Escribe aquí tu comentario'
                numberOfLines={4}
                value={opinion}
                onChangeText={text => setOpinion(text)}
                />   
            <Button 
                text={'Participar'} 
                type='primary' 
                onPress={()=> setModal(2)}
            />
            <Text style={{ textAlign: 'center' }}>
              Terminos y Condiciones</Text>
            </View>
        </Modal>
      <Modal isVisible={crrModal===1}>
        <View style={{
            backgroundColor: 'white',
            padding: 20,
            borderRadius: 12,
            width: '100%'
        }}>
        <View style={{
          flexDirection:'row', 
          justifyContent:'space-between', 
          alignItems: 'center', 
          marginBottom: 5
        }}>
          <Text style={styles.title}>
            ¡Genial!
          </Text>
          <Text style={{fontSize: 24, fontFamily: 'BRFirma-Bold'}} 
            onPress={()=>closeModal(false)}>
            X
          </Text>
        </View>
        <Text style={{fontFamily: 'BRFirma-Regular', marginBottom: 10}}>
          Por favor, déjanos tus datos para coordinar una entrevista de una hora y conocer tu opinión sobre esta aplicación.
        </Text>
          <TextInput
            elevation={4}
            style={styles.textInput}
            placeholder='Número de teléfono'
            value={telefono}
            onChangeText={text => setTelefono(text)}
          />   
            <Button 
                text={'Contáctenme'} 
                type='primary' 
                onPress={()=> {{
                  setModal(2);
                  enviarInfo();
                }}}
            />
          </View>
      </Modal>
      <Modal isVisible={crrModal===2}>
        <View
        style={{
            backgroundColor: 'white',
            padding: 20,
            borderRadius: 12,
        }}
        >
        <Text
            style={{textAlign: 'center', ...styles.title}}
        >¡Gracias por participar! 🙌</Text>
        <Button 
            text={'Regresar'} 
            type='primary' 
            onPress={()=>{closeModal(true); setModal(0)}}
        />
        </View>
      </Modal>
  </>)
}


const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      padding: 20,
      paddingLeft: 30,
      paddingRight: 30,
      margin:0,
      borderRadius: 12,
      alignItems: 'center'
    },
    textInput:{
      padding: 9,
      width: '90%',
      fontFamily: 'BRFirma-Bold',
      backgroundColor: 'white',
      borderRadius: 12, 
      marginBottom: 8
    },
    title: {
        fontFamily: 'BRFirma-Bold',
        marginBottom: 10,
        fontSize:16,
        color: '#222'
    },
    close:{
      fontSize: 24
    },
    subtitle:{
      color: '#999999',
      fontFamily: 'BRFirma-Regular',
      fontSize: 16,
      marginTop: 5,
      marginBottom: 5,
      paddingBottom: 5,
    },
    selectionContainer:{
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    text:{
      backgroundColor: '#F0F6F5',
      color: '#777',
      borderRadius: 12,
      padding: 10,
      marginLeft: 10,      
      fontFamily: 'BRFirma-Regular',
      marginBottom: 5,
    },
    selected:{
      backgroundColor: '#FF8A00',
      color: '#fff',
    },
    button:{
      backgroundColor: '#FF8A00',
      marginTop: 50,
      padding: 10,    
      alignItems: "center",
      borderRadius: 12, 
      justifyContent: 'flex-end',
      margin: 0,
      shadowColor: '#000',
      shadowOpacity: 0.12,
      shadowOffset: {width: 1, height: 10},
      shadowRadius: 20,
      borderRadius: 10,
    },
    buttonText:{
      color: '#fff',
      fontSize: 20,
      fontFamily: 'BRFirma-Regular',
    }
  });