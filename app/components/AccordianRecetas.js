import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet, 
    LayoutAnimation, Platform, UIManager,SafeAreaView,
    Image, Dimensions} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Button } from './index'
import analytics from '@react-native-firebase/analytics';

const url_ingredientes = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/ingredientes'
const { width } = Dimensions.get('window');

export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
  
    onClick=(index)=>{
        const temp = this.state.data.slice()
        temp[index].value = !temp[index].value
        this.setState({data: temp})
    }

    verReceta = () => {
        const {image, title, id, navigation} = this.props
        analytics().logEvent('boton_ver_mas', {
            receta: title
        })
        const go = () => navigation.navigate('Lista de compras')
        navigation.navigate(
        'Plato',{
            nombre: title, 
            uri: image, 
            id: id,
            goToLista: go,
        })
    }

    borrar = (i, type) => {
      const { index } = this.props;
      let copy = this.state.data;
      copy[type].splice(i, 1);

      this.setState(state => { 
        return {data: copy};
      });

      this.props.borrarIngrediente(index, i, type);
    }
    render() {

    const ingredientes = this.state.data;

    if (ingredientes.unico){
      ingredientes.unico.map((v, i) => {
        const gindx = v.equivalencias.findIndex(e => 
          e.medidaAmigable === v.unidad_convertida || e.medidaAmigable === v.unidad)
        let g = (gindx !== -1?v.equivalencias[gindx].gramos:1)*(v.cantidad_convertida===0?v.cantidad:v.cantidad_convertida)
        
        ingredientes.unico[i].gramos = g;
    });
    }

    if (ingredientes.multiple){
      ingredientes.multiple.map((v, i) => {
        const gindx = v.equivalencias.findIndex(e => 
          e.medidaAmigable === v.unidad_convertida || e.medidaAmigable === v.unidad)
        let g = (gindx !== -1?v.equivalencias[gindx].gramos:1)*(v.cantidad_convertida===0?v.cantidad:v.cantidad_convertida)
        
        ingredientes.multiple[i].gramos = g;
      });
    }

    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity style={{
            alignText: 'left',
            flexDirection:"row", 
        }}
        ref={this.accordian} style={styles.row} onPress={this.toggleExpand}>
        <Image source={{ uri: this.props.link }}
          style={{width: 110,height: 75,borderRadius: 12,marginRight: 10, marginLeft: 8}}/>
          <View style={{justifyContent: 'center',flex: 1 }}>
            <Text numberOfLines={3}
              style={{
                alignSelf: 'flex-start',
                marginBottom: 15,
                fontFamily: 'BRFirma-Bold',
                flexWrap: 'wrap',
                width: '90%'
            }}>
              {`${this.props.title}`}
            </Text>
            <View 
              style={{ 
                flexDirection: 'row',
                alignContent: 'center' }}
            >
              <Button 
                text="Ver" 
                type="secondary" 
                onPress={this.verReceta}/>
              <Text style={{
                  color: '#FF8A00',
                  fontSize: 14,
                  marginLeft: 20, 
                  alignSelf: 'center',
                  fontFamily: 'BRFirma-Regular'
                }}
                onPress={()=>{this.props.borrarReceta(this.props.index)}}
              >
                Eliminar
              </Text>
            </View>
          </View>
          <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} />
        </TouchableOpacity>
        <View style={styles.parentHr}/>
        {this.state.expanded &&  ingredientes.unico && ingredientes.unico!=undefined &&
          ingredientes.unico.length > 0 &&
          <>
            <Text style={styles.ing_titulo}>Unico Uso</Text>
            {ingredientes.unico.map((item, key) => 
              <Ingrediente k={key} key={key} i={item} borrar={this.borrar} type="unico"/>)}
          </>}
        {this.state.expanded && ingredientes.multiple && ingredientes.multiple!=undefined && 
          ingredientes.multiple.length > 0 &&
          <>
            <Text style={styles.ing_titulo}>Multiusos</Text>
            {ingredientes.multiple.map((item, key)=> 
              <Ingrediente k={key} key={key} i={item} borrar={this.borrar} type="multiple"/>)}
          </>}
      </SafeAreaView>
    )
  }

  toggleExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded : !this.state.expanded})
  }

};

const Ingrediente = ({k, i, borrar, type}) => {
  const gramos = i.gramos<1000?`${i.gramos.toFixed(0)} gr.`:`${(i.gramos/1000).toFixed(1)} kg.`

  return (
    <View key={k} 
    style={styles.ingredientes_container}>
        <View style={{flexDirection:"row",  alignItems: 'center'}}>
          <Image source = {{uri:`${url_ingredientes}/${i.imagen}`}}
            resizeMode="contain"
            style = {{ width: 80, height: 75, marginLeft: 5, marginRight: 10}}
          />
            <View style={{width:'50%'}}>
              <Text numberOfLines={2} style={styles.title_ing}>
                {i.nombre}
              </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'flex-end', width:'20%'}}>
          <Text style={{fontFamily: 'BRFirma-Regular'}}>
            {gramos}
          </Text>
          <TouchableOpacity style={{marginHorizontal: 5}} 
              onPress={() => borrar(k, type)}>
              <Icon name="delete" size={28} color="#777"/>
          </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container:{
        fontSize: 14,
        width: width
    },
    
    button:{
        width:'100%',
        height:54,
        alignItems:'center',
        paddingLeft:35,
        paddingRight:35,
        fontSize: 12,
    },
    title:{
        fontSize: 14,
        fontWeight:'bold',
    },
    itemActive:{
        fontSize: 12,
    },
    itemInActive:{
        fontSize: 12,
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        height:90,
        paddingLeft:25,
        paddingRight:18,
        alignItems:'center',
        width:'95%',
    },
    childRow:{
        flexDirection: 'row',
        justifyContent:'space-between',
    },
    parentHr:{
        height:1,
        width:'100%'
    },
    btnInActive:{
        color: 'pink'
    }, 
    btnActive:{
        color: 'green'
    },
    childHr:{
        height:1,
        width:'100%',
    },
    ing_titulo:{
      alignSelf:"flex-start",
      marginLeft: 20,
      fontSize:18,
      fontFamily: 'BRFirma-SemiBold',
      color: "#000"
    },
    ingredientes_container:{
      flexDirection:"row", 
      justifyContent: 'space-between',
      alignSelf: 'center',
      paddingBottom: 5,
      borderBottomWidth: 0.5,
      borderColor: '#EAEAEA',
      paddingHorizontal:20,
      width:'95%',
    },
    title_ing:{
      textTransform: 'capitalize',
      fontFamily: 'BRFirma-Bold',
      fontSize: 16,
      flexWrap: 'wrap'
    }
});