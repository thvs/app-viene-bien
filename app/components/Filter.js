import React from 'react';
import { 
  Text, 
  View,
  ScrollView,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import analytics from '@react-native-firebase/analytics';

export default ({data, openFilter, addRemFilter}) => {

  return (
    <View style={styles.container}>
      {/* FILTER */}
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {data.map((e, k)=> 
          {return <Text  key={k} style={styles.filter}>
            {`${e.nombre} `} 
            <View style={styles.cancelIcon}>
              <MaterialIcons name="cancel" size={22} color="#fff"
               onPress={()=>addRemFilter(e)} 
               style={{textAlign: 'center', height: 22, width:22}}/>
            </View>
          </Text>}
       )}
      </ScrollView>
      <TouchableHighlight
        underlayColor="#eee"
        style={styles.filtroIconContainer}
        onPress={()=>{openFilter(); analytics().logEvent('abrir_filtros')}}>
        <>
        <Icon name="sliders" size={20} color="#FF8A00"/>
        <Text style={styles.filtroText}>Filtros</Text>
        </>
      </TouchableHighlight>
    </View>
)}

const styles = StyleSheet.create({
  container:{
    flexDirection: 'row', 
    alignItems: 'center', 
    marginLeft: 20,
    marginRight: 15,
    marginVertical: 5,
    justifyContent: 'center',
    alignContent: 'center'

  },
  cancelIcon:{
    height:22, width:22, justifyContent: 'center', alignSelf:'center', alignItems:'center'
  },  
  filter: {
    marginLeft: 10,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 12,
    paddingRight: 10,
    borderRadius: 12,
    color: "#fff",
    fontSize: 15,
    textTransform: 'capitalize',
    backgroundColor: "#F94",
    justifyContent: 'center', alignSelf:'center', alignItems:'center'
  },
  filtroIconContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around',
    marginLeft:5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 8,
    paddingBottom: 8,
    borderRadius: 12
  },
  filtroText:{
    color: '#FF8A00',
    paddingLeft: 5,
  }
})
