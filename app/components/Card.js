import React from 'react';
import Style from '../styles/card'

import { Card } from 'react-native-paper';


//Constant ???? 
export default (props) => {
  return (
      <Card 
        style={Style.card}>
          <Card.Content 
            style={{flexGrow:1}}>
              {props.children}
          </Card.Content>
      </Card>
    )
}