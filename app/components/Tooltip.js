import React, {useState} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import Icon from 'react-native-vector-icons/FontAwesome'

export default ({uniqueKey, navigation}) => {
    const [toolTipVisible, settoolTipVisible] = useState(0)
//backgroundColor:'#EEF6F5'
    const Tip = () => {
        return (
            <View style={{borderRadius: 24, padding:8}}>
            <Text style={{fontFamily: 'BRFirma-Regular', paddingBottom: 2}}>Puedes agregar todos tus platos preferidos desde “Descubrir”</Text>
            <TouchableOpacity onPress={()=>{settoolTipVisible(false);navigation(); }} style={{alignSelf: 'flex-end'}}>
                <Text style={{borderBottomWidth: 0.5, borderColor: '#FF8A00', color:'#FF8A00', fontFamily: 'BRFirma-Regular'}}>
                    Añadir platos
                </Text>
            </TouchableOpacity>
            </View>
        )
    }
    
    return <Tooltip
            animated
            isVisible={toolTipVisible===uniqueKey}
            content={<Tip/>}
            placement="bottom"
            backgroundColor="rgba(0,0,0,0.2)"
            onClose={()=>settoolTipVisible(false)}
        >
        <TouchableOpacity onPress={()=>settoolTipVisible(uniqueKey)}>
            <Icon name="question-circle" size={24} color="#FF8A00"/>
        </TouchableOpacity>
    </Tooltip>
}

