import React, { Component } from 'react';
import { View, Dimensions, ActivityIndicator,
  StyleSheet, FlatList, 
  SafeAreaView } from 'react-native';

import Plato from './Plato'

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.39);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH / 0.8);

import API from '../api';
import analytics from '@react-native-firebase/analytics';
import { LogBox } from 'react-native';

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);

const img_receta = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/recetas/chicas';
const imgRecetaMediana = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/recetas/medianas';

export default class HorizontalSlider extends Component {
  state = {
    index: 0
  }

  constructor(props) {
    super(props);
    //Home has to send the DATA
    this.platos = props.DATA
    this.filtro = props.filtro
    this.filtroID = props.filtroID
    this.state = {
      platos: props.DATA,
      page: 2, 
      rendering: false,
      loding: false,
      total: 0,
    }
    this._renderItem = this._renderItem.bind(this)
  }

  _onPressCarousel = item => {
    analytics().logEvent('ir_a_receta' ,{ plato: item.nombre })
    const go = () => this.props.navigate('Lista de compras')
    this.props.navigate('Plato',{
      nombre: item.nombre, 
      uri: `${imgRecetaMediana}/${item.imagen2}`, 
      id: item.id,
      goToLista: go,
    })
  }
  
  _renderItem({item, index}) {
    const receta = item.receta;
    const tiempo_split = receta.tiempo_preparacion.split(' ');
    const medida = tiempo_split[1]==='minutos'? ' m':' h';
    const precio = (Math.ceil(item.costo_porcion*20)/20).toFixed(2);
    const tiempo = tiempo_split[0] + medida;
    return (
      <View key={receta.id} style={{...styles.itemContainer, marginLeft: index===0?15:0}}>
        <Plato 
          nombre  = { receta.nombre } 
          precio  = { precio } 
          uri     = { `${img_receta}/${receta.imagen3}`} 
          tiempo  = { tiempo } 
          onPress = { () => this._onPressCarousel(receta) } 
        />
      </View>
    );
  }

  _getPlatos = () => { 
    const api = new API()
    if (this.state.total!= 0 && this.state.total-(this.state.page*10)<=0)
      return;

    this.setState(() => {rendering: true})
    analytics().logEvent('explorar_recetas' ,{ filtro: this.filtroID })
    api.getPlatos('', this.filtroID, '', this.state.page, true)
      .then(res => {
        this.setState({total: res.total})
        if (res.total!==undefined &&  res.current_page===this.state.page){
          let recetas = [...this.state.platos]

          //const objIndex = recetas.findIndex(obj => obj.filtroId == this.filtroID);
          recetas= [...recetas, ...Object.values(res.data)]
          this.setState(prevState => ({
            platos: recetas,
            page: this.state.page + 1,
            rendering: false,
          })); 
        }
        else{
          this.setState({rendering: false})
        }
      }
      ).catch(err => console.log(err))
  }

  _renderFooter = () => {
    return <>
    {this.state.page ===2 || this.state.total > (this.state.page*10) ?
      <View style={{
        justifyContent: 'center', 
        alignItems: 'center', 
        height:ITEM_HEIGHT + 10, 
        marginRight: 5}}>
        <ActivityIndicator size="small" color="#FF8A00" />
      </View>
      : null}
    </>
  }

  render() {
    return (
      <SafeAreaView style={styles.carouselContainer}>
        <FlatList 
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.platos}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.state.rendering}
          ListFooterComponent={this._renderFooter}          
          progressViewOffset={200}
          onEndReachedThreshold={0.2}
          onEndReached = {this._getPlatos}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  carouselContainer: {
    marginBottom: 0,
  },
  itemContainer: {
    width: ITEM_WIDTH,
    padding: 5,
    marginRight: 15,
    shadowColor: '#202020',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 5,
  },
});
