import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg {...props} width={16} height={16} viewBox="0 0 16 16" fill="none">
      <Path
        d="M9.75 4.5a1.75 1.75 0 100-3.5 1.75 1.75 0 000 3.5zM1 2.75h7M11.5 2.75H15M4.5 9.75a1.75 1.75 0 100-3.5 1.75 1.75 0 000 3.5zM1 8h1.75M6.25 8H15M12.375 15a1.75 1.75 0 100-3.5 1.75 1.75 0 000 3.5zM1 13.25h9.625M14.125 13.25H15"
        stroke="#fff"
        strokeWidth={1.4}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default SvgComponent
