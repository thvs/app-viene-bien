export {     default as HomeScreen     } from './screens/Home';
export {  default as OnboardingScreen  } from './screens/Onboarding';
export {     default as PlatoScreen    } from './screens/Plato';
export {   default as SearchScreeen    } from './screens/Search';
export {     default as ListaScreen     } from './screens/Lista';
