class API{
    constructor(){
        this.uri = 'https://www.database.vienebien.ludik.pe/api/public/api'
    }
    
    async getPlatos(meta="", categoria="",plato="", page="", agrupar="", random=false){
        const  requestOptions = {
            method: 'GET',
            redirect: 'follow'
         };
        
        let query =  ''
        query += meta?`meta=${meta}&`:''
        query += categoria?`categoria=${categoria}&`:''
        query += page?`page=${page}&`:''
        query += agrupar?`agrupar=${agrupar}&`:''
        query += plato?`plato=${plato}`:''
        query += random?'random=true':''
        if (query.charAt(query.length-1)==='&')
            query = query.substring(0, query.length - 1);

        
        let url = `${this.uri}/${plato?'buscarPlatos':'platos'}?${query}`
        return fetch(url, requestOptions)
            .then(response => response.json())
            .then(result => result.platos?result.platos:result)
            .catch(error => console.log('error', error));
    }
    
    async getPlato(id, porcion){
        const  requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };        

        console.log(`${this.uri}/platoIndividual/${id}/${porcion}`)
        return fetch(`${this.uri}/platoIndividual/${id}/${porcion}`, requestOptions)
            .then(response => response.json())
            .then(result => result.plato)
            .catch(error => console.log('error', error));
    }

    async getFiltros(){
        const  requestOptions = {
            method: 'GET',
            redirect: 'follow'
         };
        
        return fetch(`${this.uri}/filtros`, requestOptions)
            .then(response => response.json())
            .then(result => result)
            .catch(error => console.log('error', error));
    }

    async getProductos(id){
        const  requestOptions = {
            method: 'GET',
            redirect: 'follow'
         };
        
        return fetch(`${this.uri}/productosRelacionados/${id}`, requestOptions)
            .then(response => response.json())
            .then(result => result)
            .catch(error => console.log('error', error));
    }

    async registrarComprar(compra) {
        const body = {
            "pedido" : JSON.stringify(compra)
        }
        const  requestOptions = {
            method: 'POST',
            body: JSON.stringify(body),
            redirect: 'follow',
            headers: {
                'Content-Type': 'application/json'
            },
         };
        
        return fetch(`${this.uri}/registrarCompra`, requestOptions)
            .then(response => response.json())
            .then(result => result)
            .catch(error => console.log('error', error));
    }

    async generarListadeCompra(listaDeCompra) {
        const body = {
            "listaDeCompra" : listaDeCompra
        }
        const requestOptions = {
            method: 'POST',
            body: JSON.stringify(body),
            redirect: 'follow',
            headers: {
                'Content-Type': 'application/json'
            },
         };
        return fetch(`${this.uri}/generarListadeCompra`, requestOptions)
            .then(response => response.json())
            .then(result => result.data)
            .catch(error => console.log('error', error));
    }
}

export default API
