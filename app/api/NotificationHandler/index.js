import messaging from '@react-native-firebase/messaging';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { Platform } from 'react-native';

export default class HandleNotifications{
    async saveTokenToDatabase(token) {
        // Assume user is already signed in
        const userId = auth().currentUser.uid;
        // Add the token to the users datastore
        await firestore()
        .collection('users')
        .doc(userId)
        .update({tokens: firestore.FieldValue.arrayUnion(token)})
        .catch(err=> console.log(err));
    }

    checkUpdate(){
        messaging()
        .getToken()
        .then(token => this.saveTokenToDatabase(token))
        .catch(err=> console.log(err));
        
        // If using other push notification providers (ie Amazon SNS, etc)
        // you may need to get the APNs token instead for iOS:
        // if(Platform.OS == 'ios') { messaging().getAPNSToken().then(token => { return saveTokenToDatabase(token); }); }

        // Listen to whether the token changes
        return messaging()
        .onTokenRefresh(token => this.saveTokenToDatabase(token))
    }

    registerNewUser(){
        auth()
        .signInAnonymously()
        .then(() => {
            console.log('User signed in anonymously');
            const userId = auth().currentUser.uid;
            firestore()
            .collection('users')
            .doc(userId)
            .set({ name: 'NewUser',})
            .then(()=> this.checkUpdate())
        })
        .catch(err=> console.log(err));
    }

    async requestUserPermission() {
        const authStatus = await messaging().requestPermission();
        const enabled =
          authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
          authStatus === messaging.AuthorizationStatus.PROVISIONAL;
      
        if (enabled) 
          console.log('Authorization status:', authStatus);
    }

    isFromNotification(){
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
              'Notification caused app to open from background state:',
              remoteMessage.notification,
            );
            //navigation.navigate(remoteMessage.data.type);
          });
      
          // Check whether an initial notification is available
        messaging()
        .getInitialNotification()
        .then(remoteMessage => {
            if (remoteMessage) {
            console.log(
                'Notification caused app to open from quit state:',
                remoteMessage.notification,
            );
            //setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
            }
        });
    }
}

/*
import React, { Component } from "react";
import firebase from 'react-native-firebase';

class HandleNotifications extends Component {
    constructor(props) {
        super(props);
        this.getToken = this.getToken.bind(this);
        this.requestPermission = this.requestPermission.bind(this);
        this.checkNotificationPermission = this.checkNotificationPermission.bind(this);
    }

    componentDidMount() {
        this.checkNotificationPermission();

        // setting channel for notification
        const channel = new firebase.notifications.Android.Channel(
            'channelId',
            'Channel Name',
            firebase.notifications.Android.Importance.Max
        ).setDescription('A natural description of the channel');
        firebase.notifications().android.createChannel(channel);

        // showing notification when app is in foreground.
        this.foregroundStateListener = firebase.notifications().onNotification((notification) => {
            firebase.notifications().displayNotification(notification).catch(err => console.error(err));
        });
        
        // app tapped/opened in killed state
        this.appKilledStateListener = firebase.notifications().getInitialNotification()
        .then((notificationOpen: NotificationOpen) => {
            if (notificationOpen) {
                // anything you want to do with notification object.....
            }
        });
        
        // app tapped/opened in foreground and background state
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
            // ...anything you want to do with notification object.....
        });
    }

    componentWillUnmount() {
        this.appKilledStateListener();
        this.notificationOpenedListener();
        this.foregroundStateListener();
    }

    // firebase token for the user
    async getToken(){
        firebase.messaging().getToken().then((fcmToken) => console.log(fcmToken));
    }
  
    // request permission if permission diabled or not given
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
        } catch (error) {}
    }
  
    // if permission enabled get firebase token else request permission
    async checkNotificationPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken() // call function to get firebase token for personalized notifications.
        } else {
            this.requestPermission();
        }
    }
  
    render() {
        return null;
    }
}

export default HandleNotifications;*/