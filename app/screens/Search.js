import React, { useState, useEffect }  from 'react'

import { View, Text, FlatList , Image, StyleSheet,
    ActivityIndicator, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import { SearchBar, Button, Plato, Filter, AnimatedEllipsis } from '../components'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card } from 'react-native-paper'
import Modal from 'react-native-modal';

import API from '../api'
import analytics from '@react-native-firebase/analytics';

const { width } = Dimensions.get('window');
const ITEM_WIDTH = Math.round(width * 0.39);

const img_receta = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/recetas/chicas';
const imgRecetaMediana = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/recetas/medianas';
export default ( { route, navigation: { goBack, navigate } }) => {
  const [searchQuery, setSearchQuery] = React.useState("");
  const [platos, setPlatos] = useState(() => [])
  const [loading, setLoading] = useState(true);
  const [firstPage, setFirstPage] = useState(true);
  const [filters, setFilters] = useState(()=>[]);
  const [isFilterVisible, setFilterVisible] = useState(false);
  const [showResults, setShowResults] = useState(false);
  const { goToLista } = route.params;
console.log("tes:, ", goToLista, route)
  const [rendering, setRendering] = useState(false);
  const [page, setPage] = useState(2);
  const [total, setTotal] = useState(0);
 
  const listaFiltros = [{"metaCategoria":{"nombre":"Momento del dia","categorias":[{"id":1,"nombre":"desayuno"},{"id":2,"nombre":"almuerzo"},{"id":3,"nombre":"cena"},{"id":4,"nombre":"snack"},{"id":5,"nombre":"postre"}]}},{"metaCategoria":{"nombre":"dificultad","categorias":[{"id":6,"nombre":"5 ingredientes o menos"},{"id":7,"nombre":"facil"},{"id":8,"nombre":"mediano"},{"id":9,"nombre":"dificil"}]}},{"metaCategoria":{"nombre":"edad","categorias":[{"id":10,"nombre":"para ni\u00f1os"},{"id":11,"nombre":"para adultos"},{"id":12,"nombre":"para adultos mayores"}]}},{"metaCategoria":{"nombre":"nivel de salud","categorias":[{"id":13,"nombre":"saludable"},{"id":14,"nombre":"indiferente"}]}},{"metaCategoria":{"nombre":"gastronom\u00eda","categorias":[{"id":19,"nombre":"criolla"},{"id":20,"nombre":"italiana"},{"id":21,"nombre":"asi\u00e1tica"},{"id":22,"nombre":"marina"},{"id":23,"nombre":"de la selva"}]}},{"metaCategoria":{"nombre":"dieta","categorias":[{"id":25,"nombre":"vegetariana"},{"id":26,"nombre":"vegana"},{"id":27,"nombre":"pescetariana"},{"id":28,"nombre":"indiferente"}]}},{"metaCategoria":{"nombre":"tipo de alimento","categorias":[{"id":29,"nombre":"sandwich y wraps"},{"id":30,"nombre":"jugos y batidos"},{"id":31,"nombre":"sopas y cremas"},{"id":32,"nombre":"pizzas"},{"id":33,"nombre":"ensaladas"},{"id":34,"nombre":"entradas"},{"id":35,"nombre":"segundos"},{"id":36,"nombre":"postres"}]}}]
  
  const search = () => {
    setFilters([])
    if (searchQuery!==""){
      const api = new API()
      setFirstPage(false)
      setPage(2) //Reset page
      setLoading(true)
      api.getPlatos('', '', searchQuery)
        .then(res => {
          setPlatos(res.total===0?null:Object.values(res.data))
          setTotal(res.total)
          setLoading(false)
        })
        .catch((err) => console.log(err))
      analytics().logEvent('boton_buscar', {query: searchQuery})
    }
    else{
      setFirstPage(true)
    }
     
  }

  const isInFilters = k => indexOfFilter(k) > -1
  const indexOfFilter = k => filters.findIndex(x => x.id === k)

  const toggleFilter = () => {
    analytics().logEvent('abrir_filtros')
    setFilterVisible(!isFilterVisible);
  };

  const addRemFilter = k => {
    let copyFilters = [...filters]
    if (isInFilters(k.id)) copyFilters.splice(indexOfFilter(k.id), 1)
    else{
      //analytics().logEvent('selecciona_filtro',{filtro: k.nombre})
      copyFilters.push(k)
    }
    setSearchQuery("")
    setFilters(copyFilters)
  } 

  const runFilter = () => {
    const api = new API()
    if (filters.length>0 && searchQuery===''){
      setLoading(true)
      setPage(2) // Reset Page
      setFirstPage(false)
      let cat = filters.map(obj => obj.id)

      const categoria = cat.join(",")
      api.getPlatos('', categoria)
      .then(res => {
        setTotal(res.total)
        console.log(res.total)
        setPlatos(res.status==='error' || res===undefined?{}:Object.values(res.data))
        setLoading(false)
      }
      ).catch(err =>
        console.log(err)
      )
    }
    else if (searchQuery===''){
        setShowResults(false)
        setFirstPage(true)
    }
  }

  useEffect(()=>  {
    if (showResults)
      runFilter();
  }, [filters])

  const Empty = () => (
    <View
       style={{
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 25,
        paddingLeft: 20,
        paddingRight: 20
     }}>
      <Image source={require('../assets/img/plato-vacio.png')}
        style={{
            width: 140,
            height: 140,
            marginBottom: 5,
        }}/>
      <Text 
        style={{
            textAlign: 'center',
            fontWeight: 'bold',
            fontFamily: 'BRFirma-Bold',
            marginBottom: 5,
            fontSize:15,
            color: '#111'
        }}>
         Uy... Lo sentimos
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontSize:14,
          color: '#777',
          fontFamily: 'BRFirma-Regular',
          marginBottom: 10,
        }}>
        No pudimos encontrar el plato que buscabas
      </Text>
      <Button  
        text="Intentar de nuevo"  
        type="primary" 
        onPress={()=> {setSearchQuery(''); setFirstPage(true)}}
      />
    </View>)

  const FirstPage = () => 
  (<View style={{flex: 1, backgroundColor: 'white', padding: 18,}}>
    <AllFiltros/>
  </View>)

  const goToPlato = (nombre, img, id) => (
    navigate('Plato',{
      nombre: nombre, 
      uri: img, 
      id: id,
      goToLista: goToLista,
    })
  )

  function _renderItem({item, index}) {
    const receta = item.receta

    const tiempo_split = receta.tiempo_preparacion.split(' ')
    const medida = tiempo_split[1]==='minutos'? ' m':' h'
    const tiempo = tiempo_split[0] + medida

    return (
      <View key={index} style={styles.itemContainer}>
        <Plato nombre={receta.nombre} precio={item.costo_porcion.toFixed(2)} 
          uri={`${img_receta}/${receta.imagen3}`} tiempo={tiempo}
          onPress={()=>goToPlato(receta.nombre, `${imgRecetaMediana}/${receta.imagen2}`, receta.id)}/>
      </View>
      )
  }
  const renderContent = () => (
    <Card style={styles.container}>
      <AllFiltros/>
    </Card>
  );

  const AllFiltros = () => {
    const act = c => {
      addRemFilter(c)
    }
    return <>
    <ScrollView showsVerticalScrollIndicator={false}>    

      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>

        <Text style={styles.title}>Filtrar por</Text>
        {/*!firstPage && <Text style={styles.close} 
          onPress={toggleFilter}>X</Text>*/}
      </View>
      {listaFiltros.map((f, k) =>
        <View key={k}>
          <Text style={styles.subtitle}>{f.metaCategoria.nombre}</Text>
          <View style={styles.selectionContainer}>
            {f.metaCategoria.categorias.map(c =>
              <Text key={c.id}
                style={[styles.text, isInFilters(c.id) ? styles.selected:'']}
                onPress={()=>act(c)}>
                {c.nombre}
              </Text>)}
          </View>
        </View>
      )}
    </ScrollView> 
    <TouchableOpacity style={styles.button} 
      onPress={()=>{setShowResults(filters.length>0); 
      runFilter(); 
      setFilterVisible(false)}}>
        <Text style={styles.buttonText}>
          {`Aplicar filtros`}
        </Text>
      </TouchableOpacity>
    </>
  }

  const _getPlatos = () => { 
    const api = new API()
    setRendering(total-(page*10)>-10)
    if (total-(page*10)>-10){
      if (filters.length>0 && searchQuery===''){
        let cat = []
        filters.forEach((obj) => cat.push(obj.id))

        const categoria = cat.join(",")
        api.getPlatos('', categoria, '', page)
          .then(res => {
              //const objIndex = recetas.findIndex(obj => obj.filtroId == this.filtroID);
              let newPlatos = [...platos, ...Object.values(res.data)]
              setPlatos(newPlatos)
              setPage(page+1)
              setRendering(false)
          }).catch(err => console.log(err))
      }
      else if (searchQuery!==''){
        api.getPlatos('', '', searchQuery, page)
          .then(res => {
              //const objIndex = recetas.findIndex(obj => obj.filtroId == this.filtroID);
              let newPlatos = [...platos, ...Object.values(res.data)]
              setPlatos(newPlatos)
              setPage(page+1)
              setRendering(false)
          }).catch(err => console.log(err))
      }
    }
  }

  const _renderFooter = () => {
    return <>{rendering?
      <AnimatedEllipsis 
        animationDelay={185}
        style={{
          color: '#FF8A00',
          fontSize: 20,
        }}
      />
      : null}</>
  }

  return (
    <View style={{ backgroundColor: '#FFFFFF', flexGrow:1, flex:1}}>
      <View style={{
        alignItems: 'center', 
        flexDirection: 'row',
        justifyContent:'center',
        marginBottom:5,
        margin: 20,
      }}>
      {/* Top (Search, filter) */}
        <Icon name="chevron-left"
          size={24}
          color="#000000" 
          onPress={() => goBack()}/>
        <SearchBar
          placeholder={"¿Algún plato en especial?"}
          crrQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          queryAction={search}
        />
      </View>

      {firstPage && <FirstPage/>}
      {!firstPage && 
      <>
      <Filter data={filters}  isInFilters={isInFilters}
          openFilter={toggleFilter} addRemFilter={addRemFilter}/>
      <View style={{ 
          flex: 1, 
          alignItems: 'center', 
          justifyContent: 'center',
          width: width,
        }}>
        {loading && <ActivityIndicator size="large" color="#FF8A00" />}
        {!loading && 
        (!platos || total==0 ? 
          <Empty/> : 
          <FlatList
            numColumns={2}
            showsVerticalScrollIndicator={false}
            data={platos}
            renderItem={_renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshing={rendering}
            ListFooterComponent={_renderFooter}
            onEndReachedThreshold={0.4}
            onEndReached = {_getPlatos}
          />)
          }          
      </View>
      <Modal isVisible={isFilterVisible}
        style={{justifyContent: 'flex-end', margin: 0}}
        onSwipeComplete={toggleFilter}
        swipeDirection='down'
      >
        {renderContent()}
      </Modal>
      </>}
    </View>
    )
          
}

const styles = StyleSheet.create({
  itemContainer: {
    width: ITEM_WIDTH,
    padding: 5,
    marginRight: 15,
    shadowColor: '#202020',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 5,
    elevation: 2, 
    marginBottom: 6, 
  },
  itemLabel: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 15,
    alignSelf:'center',
    paddingTop: 4
  },
  tiempo:{
    color: 'white',
    position: 'absolute', // child
    bottom: 13, // position where you want
    right: 0,
    fontSize: 14,
    padding: 6,
    paddingTop: 4,
    paddingBottom: 4,
    borderRadius:5,
    backgroundColor: '#FF8A00'
  },
  container: {
    backgroundColor: 'white',
    padding: 18,
    margin:0,
    height: 400,
    borderRadius: 12
  },
  title: {
    fontSize: 22,
    fontFamily: 'BRFirma-Bold',
  },
  close:{
    fontSize: 24,
    fontFamily: 'BRFirma-Bold',
  },
  subtitle:{
    color: '#999999',
    fontSize: 15,
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 5,
    fontFamily: 'BRFirma-Regular',
    textTransform: 'capitalize',
  },
  selectionContainer:{
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  text:{
    color: '#777',
    borderRadius: 12,
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginLeft: 8,
    marginBottom: 6,
    fontFamily: 'BRFirma-SemiBold',
    textTransform: 'capitalize',
  },
  selected:{
    backgroundColor: '#000',
    color: '#fff',
  },
  button:{
    backgroundColor: '#FF8A00',
    padding: 10,   
    paddingHorizontal: 15,
    alignItems: "center",
    borderRadius: 12, 
    justifyContent: 'flex-end',
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width: 1, height: 10},
    shadowRadius: 20,
    borderRadius: 10,
    width: '80%',
    elevation: 5,
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonText:{
    color: '#fff',
    fontSize: 20,
  }
});