import React, {useState, useEffect, useContext} from 'react'

import { View, 
  Text, 
  Image, 
  ScrollView, 
  TouchableOpacity,
  StyleSheet, 
  Dimensions, 
  ActivityIndicator,
  Linking
} from 'react-native'
import { Button } from '../../components'
import Accordian from '../../components/AccordianRecetas'
import { ListaContext } from '../../context'

import {default as Opinion} from '../../components/Opinion'
import {default as Header } from './Header'
import {default as Navegacion } from './Navegacion'
import {default as ListaCompleta} from './ListaCompleta'
import {
    CambiarProducto, 
    MoreOptions, 
    SingleProduct, 
    Terms, 
    Buy
  } from './Modals'
import { getData, storeData } from '../../api/saveToLocal'
import FontAwesome from "react-native-vector-icons/FontAwesome";
import API  from "../../api"
import analytics from '@react-native-firebase/analytics';
import { inject, observer } from "mobx-react";

const { width } = Dimensions.get('window');
/**
 * Lista de comprar 
 */
const Lista = ({ navigation, store }) => {
    const key = `menu_semanal`;
    const { lista, updateLista, getTotal } = useContext(ListaContext)
    const { menu, updateMenu } = store;

    const [nav, setNav] = useState(() => -1)
    const [crrMarket, setCrrMarket] = useState("VieneBien") //Vienebien || PlazaVea
    const [crrModal, setModal] = useState(null) 
    const [hasSentBefore, sentBefore] = useState(false) 

    const [deleted, updateDelete] = useState([])
    const [loading, setLoading] = useState(true)
    const [products, setProducts] = useState([])
    const [crrIng, setIng] = useState(null)

    const [numPedido, setNumPedido] = useState(0)

    const itemProps = {
      eliminar: (i, e, t) => borrarIngrediente(i, e, t),
      cambiar:  (i)       => cambiarProducto(i),
      deshacer: (i, r, e) => recuperar(i, r, e),
      render:   ()        => renderAccordians(),
      decubrir: ()        => navigation.navigate("Descubrir"),
      verMas:   e         => { setIng(e); setModal("SingleProduct") },
      crrMarket: crrMarket
    };

    useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => console.log());
      const key = `menu_semanal`

      getData('@Open').then(res=> sentBefore(res))

      getData(key).then(res => { 
        const newMenu = res!== null?res:[];

        getData('deleted').then(res => { 
          updateDelete(res!== null ? res: [])
          updateMenu(newMenu)
          resetLista(newMenu, ()=>{setNav(0); setLoading(false);})
        })
      }).catch(err => setLoading(false))
    
      return unsubscribe;
    }, [])

    const agregarIngredientes = (ing, lista) => {
      ing.map(v => {
        let index = lista.findIndex(e=> e.id === v.id)
        const gindx = v.equivalencias.findIndex(e => e.medidaAmigable === v.unidad)
        let g = gindx !== -1 ? v.equivalencias[gindx].gramos : 1 * v.cantidad;
  
        if (index===-1){
          const ingrediente = {
            'id': v.id,
            'nombre': v.nombre, 
            'uri': v.uri || v.imagen, 
            'cantidad': v.cantidad,
            'producto': v.userSelected ? v.producto : null,
            'gramos': g,
            'precio' : v.precio,
            'unidad' : v.unidad,
            'userSelected': v.userSelected || false,
            'equivalencias': v.equivalencias,
          }
          lista.push(ingrediente);
        }
        else{
          lista[index].gramos += g;
        }
      });
  
      return lista === undefined ? [] : lista; 
    }

    const cambiarProducto = id => {
      const api = new API();
      
      api.getProductos(id).then(
        res => {
        setProducts(res);
        setModal("ChangeProduct")
      });
    }

    const comprar = () => {
      let api = new API();
      //TODO: Enviar solo lista de productos
      const list = lista.unico.map(p => {
        return [p['nombre'], p['producto']]
      })
      list.push.apply(list, lista.multiple.map(p => {
        return [p['nombre'], p['producto']]
      }))
      console.log(list.map(n => n[1]!==null? n[1].nombre : ''))
      api.registrarComprar(list)
        .then(res => {
          const message = `¡Hola! Me gustaría realizar una compra. Mi pedido está dentro del código: ${res.codigo_compra}`
          setNumPedido(res.codigo_compra)
          const mobileNo = "950799733"
          setModal("Buy");
          analytics().logEvent('boton_comprar_cta')

          /*const timer = setTimeout( () => {
            let url = "whatsapp://send?text=" +
            message +
            "&phone=51" +
            mobileNo;
          Linking.openURL(url)
            .then(data => {
              console.log("WhatsApp Opened successfully " + data);  //<---Success
            })
            .catch(() => {
              alert("Make sure WhatsApp installed on your device");  //<---Error
            });

          }, 3000)*/
      })
    }

    const resetLista = (newMenu, finish = null) => {
      let lista = {unico: [], multiple: []};
      if (newMenu.length === 0 ) finish();
      
      for (let r of newMenu){
        let ing = r.ingredientes
        if (ing.unico.length)
          lista.unico = agregarIngredientes (
              JSON.parse (JSON.stringify(ing.unico)), 
              lista.unico
            );

        if (ing.multiple.length)
          lista.multiple = agregarIngredientes (
              JSON.parse (JSON.stringify(ing.multiple)), 
              lista.multiple
            );
      }

      //Por mejorar ! 
      const api = new API();
      let nextList = {unico: [], multiple: []};
      api.generarListadeCompra(lista.unico)
        .then(res => {
          nextList.unico = res;
          api.generarListadeCompra(lista.multiple)
            .then ( res => {
              nextList.multiple = res;
              updateLista(nextList);
              finish()
          })
        }
      );
    }

    const recuperar = (id, recepiesIds, e) => {
        setLoading(true)
        //agregrar desacer para la receta tambien
        let copyMenu = [...menu]
        
        const ingredienteExist = rid => recepiesIds.includes(rid)

        const l = e.perecible === "NO" ? 'multiple' : 'unico'

        copyMenu.forEach(r => {
          if (ingredienteExist(r.id)){
            r.ingredientes[l] = r.ingredientes[l] === undefined ? [] : r.ingredientes[l];
            r.ingredientes[l].push(e);
          }
        });

        let deleteCopy = deleted;
        deleteCopy = deleteCopy.filter(e => e.id != id)
        updateDelete(deleteCopy);
        storeData('deleted', deleteCopy)

        updateMenu(copyMenu)
        storeData(key, copyMenu)
        const finishLoading = () => {
          setNav(0);
          setLoading(false)
        }
        resetLista(copyMenu, finishLoading);
    }
    
    const borrarIngrediente = (id, e, type) => {
        setLoading(true)
        //eliminar el ingrediente de las recetas
        let copyMenu = [...menu]
        let recetasId = []

        const ingredienteExist = list => (
          list.some(el => el.id === id)
        )
        
        //Eliminar y agregar a eliminiados o eliminar completamente 
        let deleteCopy = deleted;

        if (type !== "Eliminados"){
          //Agregar recetaid para el desahacer ... 
          //Borrar recetaid cuando se haga desahacer
          const l = type === "Multiusos" ? 'multiple' : 'unico'
          copyMenu.forEach(r => {
                const list = r.ingredientes[l]
                if (ingredienteExist(list, id)) 
                  recetasId.push(r.id);
                r.ingredientes[l] = list.filter(e=>id!==e.id)
          });

          e.recetas = recetasId;
          deleteCopy.push(e)
          updateDelete(deleteCopy);
          storeData('deleted', deleteCopy)

        } else {
          deleteCopy = deleteCopy.filter(e=>id!==e.id)
          updateDelete(deleteCopy);
          storeData('deleted', deleteCopy)
        }

        updateMenu(copyMenu)
        storeData(key, copyMenu)
        const finishLoading = () => {
          setNav(0);
          setLoading(false)
        }
        resetLista(copyMenu, finishLoading);

        analytics().logEvent('boton_eliminar_ingrediente', {
            ingrediente: e.nombre
        })
    }

    const borrarReceta = index => {
      setLoading(true)

      const key = `menu_semanal`;
      let newMenu = [...menu]

      analytics().logEvent('boton_eliminar_receta', {receta: newMenu[index].nombre});
      newMenu.splice(index, 1);

      storeData(key, newMenu)
      const finishLoading = () => {
        setNav(0);
        setLoading(false)
      }
      resetLista(newMenu, finishLoading);
      updateMenu(newMenu)
    }

    const saveChange = (id, p) => {
      //Run around the menu
      let copyMenu = [];

      menu.forEach(recipe => {
        const ingredientes = recipe.ingredientes;
        ingredientes.unico.forEach((ing, k) => {
          if (ing.id === id)  { 
            ingredientes.unico[k].userSelected = true;
            ingredientes.unico[k].producto = p;
          };
        })
        ingredientes.multiple.forEach((ing, k)  =>{
          if (ing.id === id)  { 
            ingredientes.multiple[k].userSelected = true;
            ingredientes.multiple[k].producto = p;
          };
        })
        recipe.ingredientes = ingredientes;

        copyMenu.push(recipe);
      });

      const key = `menu_semanal`;     
      updateMenu(copyMenu)
      storeData(key, copyMenu)

      resetLista(copyMenu, ()=>{});
      setModal(null);
    }
    // Receta index, index de 
    const borrarIngredienteReceta = (index, i, type) => {
      setLoading(true)
      const key = `menu_semanal`;

      let newMenu = JSON.parse(JSON.stringify(menu));
      //add eliminar
      newMenu[index].ingredientes[type].splice(i, 1);

      storeData(key, newMenu).then()
      const finishLoading = () => {
        setNav(0);
        setLoading(false)
      }
      resetLista(newMenu, finishLoading);
      updateMenu(newMenu)
    }

    const renderAccordians = () => {
        const items = [];
        let k = -1;
        for (let item of menu) {
            k+=1;
            items.push(
              <Accordian key={k} 
                title   = {item.nombre}
                data    = {item.ingredientes}
                link    = {item.image}
                image   = {item.image}
                index   = {k}
                id      = {item.id}
                borrarIngrediente = {borrarIngredienteReceta}
                borrarReceta      = {borrarReceta}
                navigation={navigation}
              />
            );
        }
        return items;
    }

    const borrarAllPlatos = () => {
      setLoading(true)
      const key = `menu_semanal`;
      updateMenu([]);
      
      storeData(key, [])
      const finishLoading = () => {
        setNav(0);
        setLoading(false)
      }
      resetLista([], finishLoading);
      
      setModal(null);
      analytics().logEvent('btn_eliminar_todos_los_platos')
    }

    const show = !loading && (menu.length > 0);
    const showBuy = menu.length > 0 && nav===0;
    const showOpinion = crrModal==="Opinion"  && !hasSentBefore 

    return (
    <View style={{ backgroundColor: 'white', flex:1 }}>
      <View style={{ flex:1 }}>
        <ScrollView showsVerticalScrollIndicator = { false }>
          <Header
            crr           = { crrMarket }
            changeMarket  = { setCrrMarket }
            show          = { show }
            showOptions   = { () => setModal("ViewOptions") }
            getTotal         = { getTotal }
          />
          <View style={[styles.container_ing, {marginBottom: show ? 5 : 45}]}> 
            {show && 
              <Navegacion
                setVistaCompleta  = { () => setNav(0) }
                setVistaPorPlatos = { () => setNav(1) }
                crrNav            = { nav }
              />
            }
            {loading && <ActivityIndicator size="large" color="#FF8A00" />}
            {!loading &&  
              <Sections 
                empty   = { menu.length === 0 }
                crr     = { nav }
                lista   = { lista }
                deleted = { deleted }
                promps  = { itemProps }
            />}
          </View>
        </ScrollView>
        { showBuy && 
          <View style={styles.under_container}>
            <Button type = "child" onPress = { comprar }> 
              <Text style={styles.buy_button}>
                  Comprar <FontAwesome name="whatsapp" size={18}/>
              </Text>
            </Button>
            <TouchableOpacity 
              onPress = { () => setModal("Terms") }
              style = {{ flexDirection:"row", marginTop: 5 }}
            >
              <FontAwesome name="truck" size={20} color={"#FF8A00"} />
              <Text style={styles.terms_text}>
                  Políticas de pago y envío
              </Text>
            </TouchableOpacity>
          </View> }
      </View>
      {showOpinion &&
        <Opinion 
          closeModal={
            done => {
              storeData('@Open', done); 
              setModal(null)
              sentBefore(done)
          }}
        />
      }
      <MoreOptions
        show      = { crrModal === "ViewOptions" }
        hide      = { () => setModal(null) }
        deleteAll = { borrarAllPlatos }
      />
      <CambiarProducto
        show      = { crrModal === "ChangeProduct" }
        products  = { products }
        hide      = { () => setModal(null) }
        save      = { (i, p) => saveChange(i, p) }
      />
      <Terms 
        show = { crrModal === "Terms" }
        hide = { () => setModal(null) }
      />
      <SingleProduct 
        show    = { crrModal === "SingleProduct" }
        cambiar = { cambiarProducto }
        item    = { crrIng }
        hide    = { () => setModal(null) }
      />
      <Buy 
        show = { crrModal === "Buy" }
        hide = { () => setModal(null) }
        numero = { numPedido }
      />
    </View>)
}


const EmptyList = ({goToDescubrir}) => (
    <View style={styles.empty_container} >
        <Image style={styles.empty_img} source={require('../../assets/img/verduras.png')}/>
        <Text style={styles.empty_title}>
            Tu lista de compras está vacia
        </Text>
        <Text style={styles.empty_text}>
          Se creará una vez que añadas uno o más platos a tu menú semanal
        </Text>
        <Button  
            text="Añadir platos"  
            type="primary" 
            onPress={goToDescubrir}
        />
    </View>
)

const Sections = ({empty, crr, lista, deleted, promps}) => {
  return (
    <>
    { empty ? 
      <EmptyList
        goToDescubrir={promps.decubrir}
      /> : 
        ( crr === 0 ) ? 
          <>
            <ListaCompleta 
              type      = "Frescos"
              crrlista  = {lista.unico} 
              {...promps} 
            />
            <ListaCompleta
              type      = "Almacén"
              crrlista  = {lista.multiple} 
              {...promps} 
            />
            <ListaCompleta
                type      = "Eliminados"
                crrlista  = {deleted}
                {...promps} 
              />
          </> 
      : ( crr === 1 ) && 
        <View style={{flex:1, paddingTop:5}}>
          {promps.render()}
        </View>
  }
  </>
)};

export default inject("store")(observer(Lista));

const styles = StyleSheet.create({
    empty_container:{
      justifyContent: 'center',
      alignItems: 'center',
      margin: 25,
      paddingLeft: 20,
      paddingRight: 20
    },
    empty_img:{
      width: 200,
      height: 120,
      borderRadius: 12,
      marginBottom: 5,
},
empty_title:{
  textAlign: 'center',
  fontFamily: 'BRFirma-Bold',
  marginBottom: 5,
  fontSize:15,
  color: '#111'
},
empty_text:{
  textAlign: 'center',
  fontSize:14,
  color: '#777',
  marginBottom: 10,
},
    container: {
      backgroundColor: 'white',
      padding: 20,
      paddingHorizontal: 25,
      margin:0,
      borderRadius: 12,
      alignItems: 'center'
    },
    title: {
        marginBottom: 10,
        fontSize:16,
        color: '#222',
        fontFamily: 'BRFirma-Bold'
    },
    buttonText:{
      color: '#fff',
      fontSize: 20,
    },
    container_ing:{
    padding: 5,
    alignItems: 'center', 
    justifyContent: 'center', 
    width: width
  },
  under_container:{
    backgroundColor: "#fff", 
    alignItems: 'center',  
    paddingTop: 12, 
    paddingBottom: 15
  },
  buy_button:{
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontFamily:'BRFirma-Bold'
  },
  terms_text:{
    fontFamily: 'BRFirma-SemiBold', 
    color:"#FF8A00", 
    marginHorizontal: 5,
    textDecorationLine: 'underline'
  }
});