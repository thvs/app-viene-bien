import React, {useEffect, useState} from 'react'
import { 
    View, 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Image,
    SafeAreaView,
    Dimensions,
    FlatList,
    ActivityIndicator
 } from "react-native"; 

//Producto no 

import { Button } from '../../components'

import Modal from 'react-native-modal';
import Icon from "react-native-vector-icons/MaterialIcons";
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import CheckBox from '@react-native-community/checkbox';
const { width } = Dimensions.get('window');

const VistaProducto = ({ products, checkbox, update}) => {    
    const renderItem = ({item, index}) => {
        return (
        <View style={{width:'50%'}}>
            <CheckBox
                disabled        = { false }
                boxType         = { 'circle' }
                tintColors      = {{ true: '#FF8A00', false: '#FF8A00' }}
                value           = { checkbox[index] }
                style           = {{ marginTop: 8, alignSelf: 'flex-end', marginRight: 5 }}
                onValueChange   = { () => update(index) }
            />
            <Image 
                style       = {styles.image_product}
                source      = {{uri: item.imagen.replace('450-450', '150-150')}}
                resizeMode  = "contain"
            />
            <View style={{width:'90%', margin: 0, padding: 0}}>
                <Text style={{color: '#FF8A00',fontFamily: 'BRFirma-Bold', marginTop: 5}}>
                    {`S/${ item.price>0 ? item.price.toFixed(2) :'0.01' }`}
                </Text>
                <Text style={styles.text_product}>
                    {item.nombre}
                </Text>
                <Text style={{color: '#FF8A00', fontFamily: 'BRFirma-Regular'}}>
                    {`${item.cantidad} ${item.unidadCantidad}`}
                </Text>
            </View>
        </View>
        )
    }

    return(
        <SafeAreaView style={{height: 240}}>
            <FlatList
                data={products}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                numColumns={2}
                showsHorizontalScrollIndicator={false}
                columnWrapperStyle={style.row}
            />
        </SafeAreaView>
)};

const style = StyleSheet.create({
    row: {
      flex: 1,
    },
    container: {
        marginTop: 10,
        width: width,
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: 10,
        justifyContent: 'space-evenly',
    },
});

const VistaCantidad = ({nombre, precio, cantidad, setCantidad, img, fixedChange}) => {
    const newPrecio = precio * cantidad;

    return <View style={{
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: '65%'}}
    >
        <Image source = {{uri: img}}
            resizeMode="contain"
            style = {styles.image_product}
        />
        <Text style={styles.text_product}>
            {nombre}
        </Text>
        <View style={{flexDirection: 'row'}}>
            <FontAwesome 
                name    = "minus-circle" 
                color   = {"#FF8A00"} 
                size={24} style={{paddingRight:5, alignSelf:'center'}}
                onPress={()=> (
                    cantidad-fixedChange >= 0.1 
                        ? setCantidad(cantidad-fixedChange) : '')
                    }
            />
            <View style={styles.cantidad_container}>
                <Text style={styles.cantidad_text}>
                    {cantidad.toFixed(2)}
                </Text>
                <Text style={styles.cantidad_unidad}>kg.</Text>
            </View>
            <FontAwesome 
                name="plus-circle" 
                color={"#FF8A00"} 
                size={24} 
                style={{paddingLeft:5, alignSelf:'center'}}
                onPress={()=> setCantidad(fixedChange+cantidad)}
            />
        </View>
        <Text style={styles.precio}>
            Precio: S/{newPrecio.toFixed(2)}
        </Text>
    </View>
}

const NavItem = ({showView, crr, texto}) => {
    const style = StyleSheet.create({
        texto: {
            color: crr ? '#FF8A00':"#777", 
            fontFamily: crr? "BRFirma-Bold":"BRFirma-Regular",
            marginTop: 1,
            borderBottomColor: crr ? '#FF8A00':"#777", 
            alignSelf:"center", 
            borderBottomWidth: crr ? 2:1, 
        },
    })

    return (
    <TouchableOpacity onPress={showView}>
            <Text style={style.texto}>
                {texto}
            </Text>
        </TouchableOpacity>
    )
}


export const CambiarProducto = ({show, hide, products, save}) => {
    const [nav, setNav] = useState("Producto");
    const [cantidad, setCantidad] = useState(null);
    const [product, setProduct] = useState(null);

    const [toggleCheckBox, setToggle] = useState(() =>
        Array(products.length).fill(true)
    )

    const h = 165+(80*3);

    const updateToggle = k => {
        let copy = toggleCheckBox;
        console.log(JSON.stringify( products[k], null, 2))
        copy.fill(false);
        copy[k] = true;
        setToggle(copy);
        setProduct(products[k]);
        setCantidad(products[k].cantidadkg ? products[k].cantidadkg : 2.5);
    }

    const Header = () => (
        <>
        <View style={styles.container_cancel}>
                <Text style={styles.modal_text}>
                    {nav==="Producto"?'Cámbialo por:': "Cambia la cantidad:"}
                </Text>
                <TouchableOpacity onPress={()=>{hide(); setNav("Producto");}}>
                    <Text style={styles.text_cancel}>
                        Cancelar
                    </Text>
                </TouchableOpacity>
        </View>
        <View style={style.container}>
            <NavItem
                showView= {()=>setNav("Producto")}
                crr = {nav === "Producto" }
                texto = {"MARCA"}
            />
            <NavItem
                showView    = {()=>setNav("Cantidad")}
                crr         = {nav === "Cantidad"}
                texto       = {"CANTIDAD"}
            />
        </View>
        </>
    )

    const Footer = () => (
        <View style={styles.container_actions}>
            <Button 
                extraStyle  = {styles.extra_button}
                extraText   = {{fontSize: 16}}
                text        = {'Guardar'} 
                type        = 'primary' 
                onPress     = { () => {
                    const copyProduct = product;
                    console.log(copyProduct)
                    if (copyProduct.cantidadkg)
                        copyProduct.cantidad = cantidad
                    if (product){
                        save(product.idIngrediente, copyProduct);
                    }
                    setNav("Producto");
                }}
            />
        </View>
    )

    const showCantidad = nav === "Cantidad" &&  product!==null 
    return (
    <Modal 
        isVisible={show}
        style={{ justifyContent: 'flex-end', margin: 0 }}
        propagateSwipe={true}
    >
        <View style={{...styles.modal_container, height: h, width:width}}>
            <Header/>
            {nav ==="Producto" && 
                <VistaProducto
                    products    = { products }
                    checkbox    = { toggleCheckBox }
                    update      = { updateToggle }
                />
            }
            {(showCantidad && product.en_kilos == 1) && 
                <VistaCantidad
                nombre      = { product.nombre }
                precio      = { product.price }
                img         = { product.imagen }
                setCantidad = { setCantidad }
                cantidad    = { cantidad }
                fixedChange = { product.cantidadkg ? product.cantidadkg : 1.5}
            />}
            {(showCantidad && product.en_kilos == 1) &&
                <Text>
                    Este producto no se puede modificar de cantidad.
                </Text>
            }
            <Footer/>
        </View>
    </Modal>
)};

export const MoreOptions = ({deleteAll, hide, show}) => (
<Modal 
    isVisible={show}
    style={{justifyContent: 'flex-end', margin: 0}}
    onBackdropPress={hide}
    onSwipeComplete={hide}
    swipeDirection='down'>
    <View style={styles.modal_container}>
    <View style={styles.modal_line}/>
    <View style={{justifyContent: 'space-evenly', flex:1}}>
        <TouchableOpacity
            accessibilityLabel="Eliminar Platos"
            onPress={deleteAll}
            style={{flexDirection: 'row', justifyContent: 'space-between'}}
        >
            <Text style={styles.modal_text}>
                Eliminar Platos
            </Text>
            <Icon name="delete" size={26} />
        </TouchableOpacity>
        </View>
    </View>
</Modal>
)

// TODO: Style the text
// TODO: Add the images of payment methods
export const Terms = ({show,hide}) => (
    <Modal  isVisible={show}
            onBackdropPress={hide}
            onSwipeComplete={hide}
    >
      <View style={styles.container}>
        <Text style={styles.title}>
            Política de pago y envíos
        </Text>
        <View style={styles.texto_terms}>
            <Text style={styles.subtitulo}>Tiempo de envíos</Text>
            <Text style={styles.text}>24h desde el pedido</Text>
            <Text style={styles.subtitulo}>Costo de envío</Text>
            <Text style={styles.text}>Se calcula según la distancia del envío.</Text>
            <Text style={styles.subtitulo}>Zona de cobertura</Text>
            <Text style={styles.text}>La Molina, Miraflores, San Borja, San Isidro, Santiago de Surco y Surquillo</Text>
            <Text style={styles.subtitulo}>Medios de pago</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <Image 
                    source={require('../../assets/img/Visa.png')}
                />
                 <Image 
                    source={require('../../assets/img/Yape.png')}
                />
                 <Image 
                    source={require('../../assets/img/Lukita.png')}
                />
            </View>
        </View>
        <Text style={styles.a_link}>Más información</Text>
        <Button 
            text={'Aceptar'} 
            type='primary' 
            onPress={hide}
        />
        </View>
    </Modal>
)

export const SingleProduct = ({show, hide, item, cambiar}) => {
    const e = item ? item.producto : null;
    
    return <Modal  isVisible={ show }
            style={{justifyContent: 'flex-end', margin: 0}}
            onBackdropPress={hide}
            onSwipeComplete={hide}
    >
        {item && <View style={[styles.modal_container, {height: 190}]}>
            <View style={styles.container_cancel}>
                <Text style={styles.modal_text}>
                    Sobre el producto
                </Text>
                <TouchableOpacity onPress={hide}>
                    <Icon name = "close" size={28} />
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:"row",  alignItems: 'center', marginTop: 10}}>
                <Image 
                    source = {{uri: e.imagen.replace('450-450', '150-150')}}
                    resizeMode="contain"
                    style = {styles.image_product}
                />
                <View style={{width:'80%'}}>
                    <Text style={[styles.text_product, {marginBottom: 5}]}>
                        {e.nombre}
                    </Text>
                    <View style={{width:'80%'}}>
                        <Text style={{color: '#FF8A00',fontFamily: 'BRFirma-Regular'}}>
                            {`Precio: S/${ e.price>0 ? e.price.toFixed(2) :'0.01' }`}
                        </Text>
                        <Text style={{color: '#777777', fontFamily: 'BRFirma-Regular'}}>
                            {`Cantidad: ${e.cantidad} ${e.unidadCantidad}`}
                        </Text>
                    </View>
                </View>
            </View>
            <TouchableOpacity onPress={ () => cambiar(item.id)}>
                <Text style={[styles.a_link, {marginTop: 10}]}>
                    Cambiar marca <Icon name="chevron-right"/>
                </Text>
            </TouchableOpacity>
        </View>}
        <></>
    </Modal>
}


export const Buy = ({show, hide, numero}) => {
    return <Modal  isVisible={show}
            onBackdropPress={hide}
            onSwipeComplete={hide }
        >
            <View style={styles.container}>
                <ActivityIndicator size="small" color="#FF8A00" />
                <Text style={styles.title}>
                   {`Serás redirigido al Whatsapp con el N° de pedido #${numero}`}
                </Text>
                <Text style={styles.texto_terms}>
                    *Si olvidaste algún producto, puedes pedirlo desde ahí.
                </Text>
                <Button text={'cancelar'} type='primary' 
                    onPress={hide}/>
            </View>
        </Modal>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20,
        paddingHorizontal: 25,
        margin:0,
        borderRadius: 12,
        alignItems: 'center'
    },
    title: {
        fontSize:16,
        color: '#222',
        fontFamily: 'BRFirma-Bold'
    },
    texto_terms:{
        color: '#777',
        margin: 10,
        fontFamily: 'BRFirma-Regular'
    },
    modal_container: {
        display: 'flex',
        width: width,
        backgroundColor: 'white',
        padding: 25,
        height: 130,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12
    },
    modal_text:{
        fontFamily:'BRFirma-SemiBold', 
        fontSize:21, 
    },
    modal_line:{
        borderBottomColor: 'black',
        borderBottomWidth: 3,
        alignSelf: 'center',
        borderBottomColor: '#999',
        width: '50%',
        marginBottom: 15
    },
    image_product: { 
        width: 75, 
        height: 75,
        marginRight: 8,
        alignSelf: 'center',
    },
    text_product: {
        textTransform: 'capitalize',
        fontFamily: 'BRFirma-Bold',
        fontSize: 16,
        flexWrap: 'wrap'
    },
    container_actions:{
        flexDirection:"row",  
        justifyContent: 'center', 
        width:'100%',
        alignItems: 'flex-end',
        flex:1,
        height: 10,
      },
    text_cancel: {
        fontFamily: 'BRFirma-Bold', 
        fontSize: 14, 
        color: "#FF8A00",
        alignSelf: 'center',
        margin:0,
        padding:0
    }, 
    container_cancel:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        width: '100%'
    },
    a_link:{
        fontSize: 14,
        fontFamily: 'BRFirma-SemiBold', 
        color:"#FF8A00", 
        marginTop: 5,
        textDecorationLine: 'underline'
    },
    extra_button: {
        marginTop: 0,
        width: '50%', 
        alignSelf: 'flex-end',
    }, 
    precio: {
        fontSize: 16, 
        marginBottom:10, 
        marginLeft:'5%', 
        fontFamily: 'BRFirma-Bold', 
    },
    product:{
        marginTop: 10,
        paddingVertical: 10,
        height: 190
    },
    cantidad_container:{
        backgroundColor: "#F5F5F5",
        borderRadius: 12,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
        width: 80,
        marginVertical: 15,
        marginHorizontal: 10
    },
    cantidad_text:{
        fontFamily: "BRFirma-Bold",
        fontSize: 16,
    },
    cantidad_unidad:{
        fontSize: 13, 
        color: "#999",
        fontFamily: "BRFirma-Regular"
    },
    subtitulo: {
        fontFamily: "BRFirma-Bold",
        color: "#999",
        fontSize: 14,
        marginTop: 10,
        marginBottom: 4,
    },
    text: {
        fontFamily: "BRFirma-Regular",
        color: "#999",
    }
})