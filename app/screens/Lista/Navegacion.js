import React from 'react'
import { 
    View, 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Dimensions
 } from "react-native";

 const { width } = Dimensions.get('window');


const NavItem = ({showView, crr, texto}) => {
    const style = StyleSheet.create({
        texto: {
            color: crr ? '#FF8A00':"#777", 
            fontFamily: crr? "BRFirma-Bold":"BRFirma-Regular",
            marginTop: 1,
            borderBottomColor: crr ? '#FF8A00':"#777", 
            alignSelf:"center", 
            borderBottomWidth: crr ? 2:1, 
        },
    })

    return (
    <TouchableOpacity onPress={showView}>
            <Text style={style.texto}>
                {texto}
            </Text>
        </TouchableOpacity>
    )
}

export default ({setVistaCompleta, setVistaPorPlatos, crrNav}) => (
    <View style={styles.container}>
        <NavItem
            showView= {setVistaCompleta}
            crr = {crrNav === 0 }
            texto = {"VISTA COMPLETA"}
        />
        <NavItem
            showView= {setVistaPorPlatos}
            crr = {crrNav === 1 }
            texto = {"POR PLATOS"}
        />
    </View>
);


const styles = StyleSheet.create({
    container: {
        width: width,
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: 10,
        justifyContent: 'space-evenly',
    },
})