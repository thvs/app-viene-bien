import React, {Component} from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Image,
    Dimensions,
    LayoutAnimation, Platform, UIManager,SafeAreaView,
 } from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import AppleStyleSwipeableRow from './AppleStyleSwipeableRow';

const url_ingredientes = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/ingredientes'
const { width } = Dimensions.get('window');

const displayUnidad = {
    'litro' : 'L',
    'mililitro' : 'ml',
    'cucharadita' : 'cdta',
    'cucharada' : 'cda',
    'taza' : 'tza',
    'unidad' : 'ud',
}

class Accordian extends Component{
    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
  
    onClick=(index)=>{
        const temp = this.state.data.slice()
        temp[index].value = !temp[index].value
        this.setState({data: temp})
    }

    render() {

    const crrlista = this.state.data;
    const { eliminar, cambiar, deshacer, verMas, crrMarket } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity style={{
            alignText: 'left',
            flexDirection:"row", 
        }}
            ref={this.accordian} style={styles.row} onPress={this.toggleExpand}>
            <Text style={styles.ing_titulo}>
                {`${this.props.title}`}
            </Text>
          <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} />
        </TouchableOpacity>
        <View style={styles.parentHr}/>
        {this.state.expanded && crrlista.map((e, k) => {
            let precio = e.producto?.price * 
                ( e.producto?.cantidadkg && e.producto?.unidadCantidad === 'gr' ? 
                (e.producto.cantidad > 20 ? e.producto.cantidad/1000 : e.producto.cantidad) : 1)
                || e.precio || 0.01
            precio = crrMarket === 'VieneBien' ? precio * 0.95 : precio;

            const peso = e.producto?.en_kilos && e.producto.unidadCantidad === 'gr' ? 
                    e.producto?.cantidad > 500 ? `${(e.producto.cantidad/1000).toFixed(1)} kg.`: `${(e.producto.cantidad).toFixed(1)} kg.`
                    :  e.producto?.cantidad > 100 &&  e.producto?.unidadCantidad === 'gr' ? `${(e.producto.cantidad /1000).toFixed(1)} kg`
                        : `${e.producto?.cantidad} ${e.producto?.unidadCantidad in displayUnidad 
                                ? displayUnidad[e.producto?.unidadCantidad]
                                : e.producto?.unidadCantidad}`
            || 0


            const action = [ this.props.title==="Eliminados" ? 
                () => deshacer(e.id, e.recetas, e) : 
                () => cambiar(e.id), 
                () => eliminar(e.id, e, this.props.title) 
        ]
        const types   = [ this.props.title === "Eliminados" ? 'recuperar' : 'cambiarmarca', 'eliminar' ]

        return (
            <AppleStyleSwipeableRow 
                type    = { types }
                action  = { action }
                key     = { k }
                disable = { !e.producto }
            >
               <Ingrediente 
                e       = {e} 
                precio  = {precio} 
                peso    = {peso}
                verMas  = { verMas }
                type    = {this.props.title}
               />
            </AppleStyleSwipeableRow>
        )})}
      </SafeAreaView>
    )
  }

  toggleExpand=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded : !this.state.expanded})
  }

};

const Ingrediente = ({e, precio, peso, type, verMas, openSwipable}) => {
    const styleTitle = {
        textDecorationLine: type==="Eliminados"?"line-through":"none"
    }
    
    return (
    <View style={{...styles.listItem}}>
    <View style={{flexDirection:"row",  alignItems: 'center'}}>
        <Image source = {{uri:`${url_ingredientes}/${e.uri}`}}
            resizeMode="contain"
            style = {styles.image_ing}
        />
        <View style={{width:'60%'}}>
            <Text style={[styles.nombre_ing, styleTitle, 
               { color:  e.producto && type!=="Eliminados" ? "#000" : "#999"}]}>
                {e.nombre}
            </Text>
            <View style={styles.containerPrice}>
                <Text style={{color: type==="Eliminados" || !e.producto?'#999':'#FF8A00', fontFamily: 'BRFirma-Regular'}}>
                    {`S/${precio.toFixed(2)}`}
                </Text>
                {e.producto &&
                <Text style={{color: type==="Eliminados" || !e.producto?'#999':'#777', fontFamily: 'BRFirma-Regular'}}>
                    {peso}
                </Text>}
               {e.producto !== null &&
                <TouchableOpacity onPress={ () => verMas(e)}>
                    <Text style={styles.a_link}>
                        Ver más
                    </Text>
                </TouchableOpacity>}
            </View>
        </View>
        </View>
        {e.producto ? 
            <View style={styles.containerIcon}>
                <TouchableOpacity 
                    onPress={openSwipable}>
                    <Icon name="mode-edit" size={26} color="#777"/>
                </TouchableOpacity>
            </View> :
            <View style={styles.containerIcon}>
                <Icon name="block" color={"#EA4335"} />
                <Text style={styles.sin_stock_text}>
                    Sin Stock
                </Text>
            </View>
        }
    </View>
)}

export default ({type, crrlista, eliminar, cambiar, deshacer, verMas, crrMarket}) => {
    if(crrlista && crrlista.length > 0)
        return <>
            <Accordian 
                title   = {type}
                data    = {crrlista}
                eliminar = {eliminar}
                cambiar = {cambiar}
                deshacer = {deshacer}
                verMas  = {verMas}
                crrMarket = {crrMarket}
            />
        </>
    else   
        return null
};

const styles = StyleSheet.create({
  rectButton: {
    flex: 1,
    height: 80,
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  separator: {
    backgroundColor: 'rgb(200, 199, 204)',
    height: StyleSheet.hairlineWidth,
  },
  fromText: {
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  messageText: {
    color: '#999',
    backgroundColor: 'transparent',
  },
  dateText: {
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 20,
    top: 10,
    color: '#999',
    fontWeight: 'bold',
  },
  container: {
        fontSize: 14,
        width: width
    },
listItem: {
    alignItems: 'center',
    flexDirection:"row", 
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: width, 
    paddingHorizontal:20
},
image_ing:{ 
    width: 75, 
    height: 75,
    marginRight: 10,
    marginLeft: 5,
},
nombre_ing:{
    textTransform: 'capitalize',
    fontFamily: 'BRFirma-Bold',
    fontSize: 16,
    flexWrap: 'wrap',
    marginBottom:1,
},
containerPrice:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent:'space-between',
    width:'100%'
},
containerIcon:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent:'flex-end', 
    width:'15%'
},
containerBlockIcon:{
    flexDirection: 'row',  
    alignItems: 'center', 
},
rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20,
    backgroundColor: '#FF8A00'
},
iconSwipeItem:{
    paddingLeft:6, 
    paddingBottom: 1
},
listItemText: {
    fontFamily: 'BRFirma-SemiBold',
    fontSize: 10,
    color: '#fff',
},
ing_titulo:{
    marginLeft: 20,
    fontSize:18,
    fontFamily: 'BRFirma-SemiBold',
    color: "#000"
},
a_link:{
    fontSize: 13,
    fontFamily: 'BRFirma-SemiBold', 
    color:"#FF8A00", 
    marginTop: 5,
    textDecorationLine: 'underline'
},
sin_stock_text:{
    color:"#EA4335", 
    marginLeft: 1, 
    fontFamily: 'BRFirma-SemiBold',
},
row:{
    flexDirection: 'row',
    justifyContent:'space-between',
    height:45,
    paddingLeft:10,
    paddingRight:10,
    alignItems:'center',
    width:'95%',
},
});