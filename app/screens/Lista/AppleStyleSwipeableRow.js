import React, { Component } from 'react';
import { Animated, StyleSheet, Text, View, I18nManager } from 'react-native';

import { RectButton } from 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/MaterialIcons";
import Swipeable from 'react-native-gesture-handler/Swipeable';

const content = {
  cambiarmarca: {
      icon: "swap-horiz",
      firstText: "Cambiar",
      secondText: "marca"
  },
  recuperar: {
      icon: "history",
      firstText: "Recuperar",
      secondText: ""
  },
  eliminar: {
      icon: "delete",
      firstText: "Eliminar",
      secondText: ""
  }
}

export default class AppleStyleSwipeableRow extends Component {
  renderRightAction = (values, color, x, progress, action, show, open) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });

    const pressHandler = () => {
      action();
      this.close();
    };

    if (show)
      return (
        <Animated.View style={{ flex: 1, transform: [{ translateX: trans }] }}>
          <RectButton
            style={[styles.rightAction, { backgroundColor: color }]}
            onPress={pressHandler}>
            <Icon name={values.icon} size={26} color="#fff" style={styles.iconSwipeItem}/>
            <Text style={styles.listItemText}>{values.firstText}</Text>
            <Text style={styles.listItemText}>{values.secondText}</Text>
          </RectButton>
        </Animated.View>
      );
    else
        return <></>
  };
  renderRightActions = progress => {
    const values = {
      left: content[this.props.type[0]],
      right: content[this.props.type[1]]
    } 
    const { disable } = this.props;

    return <View
      style={{
        width: 160,
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
      }}>
      {this.renderRightAction(
        values.left, '#FF8A00', 128, progress, this.props.action[0], !disable
      )}
      {this.renderRightAction(
        values.right, '#FF8A00', 64, progress, this.props.action[1], true
      )}
    </View>
  };
  updateRef = ref => {
    this._swipeableRow = ref;
  };
  close = () => {
    this._swipeableRow.close();
  };
  open = () => {
    this._swipeableRow.openRight();
  }

  render() {
    const { children } = this.props;
    const render = this.renderRightActions

    return (
      <Swipeable
        ref={this.updateRef}
        friction={2}
        enableTrackpadTwoFingerGesture
        rightThreshold={40}
        renderRightActions={render}
        >
        {React.cloneElement(children, { openSwipable: this.open })}
      </Swipeable>
    );
  }
}

const styles = StyleSheet.create({

  actionText: {
    color: 'white',
    fontSize: 16,
    backgroundColor: 'transparent',
    padding: 10,
  },
  rightAction: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20,
    backgroundColor: '#FF8A00'
},
iconSwipeItem:{
    paddingLeft:6, 
    paddingBottom: 1
},
listItemText: {
    fontFamily: 'BRFirma-SemiBold',
    fontSize: 10,
    color: '#fff',
},
});
