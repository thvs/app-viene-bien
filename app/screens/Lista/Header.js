import React from 'react'
import { 
    View, 
    Text, 
    TouchableOpacity,
    StyleSheet,
    Dimensions
 } from "react-native";

import { PlazaVea, VieneBien } from '../../components'

const { width } = Dimensions.get('window');

const Logos = ({tienda, activo, precio, swap}) => {
    const style = {
        backgroundColor: activo? "#FF8A00" : "#F0F6F5",
        color: activo ? "#fff" : "#777",
        fontFamily: 'BRFirma-Bold', 
    }

    const decimal = precio - Math.floor(precio)

    return (
    <TouchableOpacity 
        onPress={swap}
        style = {{...styles.mercado, ...style}}
    >
        {tienda === "PlazaVea" && 
            <PlazaVea color={style.color}/>}
        {tienda === "VieneBien" &&           
            <VieneBien color={style.color}/>
        }
        <Text style={style}>
            {` S/${precio-decimal}`}
            <Text style={{fontSize: 10}}>
                {`.${(decimal.toFixed(2)*100).toFixed(0)}`}
            </Text>
        </Text>
    </TouchableOpacity>
)}

export default (props) => {
    const { show, showOptions, getTotal, crr } = props;
    const { changeMarket } = props;

    const crrTotal = getTotal(crr==="VieneBien")
    return (
    <View style={styles.out_container}>
        <View style={styles.container}>
            <Text style={styles.titulo}>
                Mi lista de compras
            </Text>
            {show && 
                <TouchableOpacity 
                    style={styles.options_container}
                    onPress={showOptions}
                >
                    <Text style={styles.options}>
                        {'\u22EE'}
                    </Text>
                </TouchableOpacity>
            }
        </View>
        {show && 
            <>
                <Text style={styles.seleccion_titulo}>Selecciona un supermercado:</Text>
                <View style={styles.seccion_container}>
                    <Logos 
                        tienda  = { "VieneBien" }
                        activo  = { crr === "VieneBien" }
                        swap    = {()=>changeMarket("VieneBien")}
                        precio  = { getTotal(true)}
                    />
                    <Logos 
                        tienda = {"PlazaVea"}
                        activo = {crr==="Plaza Vea"}
                        swap   = {()=>changeMarket("Plaza Vea")}
                        precio = { getTotal(false) }
                    />
                </View>
                <Text style={styles.precio}>
                    {`Total a pagar: S/${crrTotal.toFixed(2)}`}
                </Text>
                <View style={styles.lineStyle}/>
            </>
        }
    </View>
)};


const styles = StyleSheet.create({
    out_container: {
        alignItems: 'center',
        marginBottom: 12,
        marginTop: 20,
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 12,
        marginTop: 20,
    },
    titulo:{
        fontFamily: 'BRFirma-Bold', 
        fontSize: 18
    },
    options_container: {
        paddingHorizontal: 10,
        position: 'absolute',
        left: width - 135,
    },
    options: {
        fontSize:24, 
        color:'#FF8A00', 
        fontFamily: 'BRFirma-Bold', 
    },
    seleccion_titulo:{
        fontFamily: 'BRFirma-Bold', 
        fontSize: 13,

    },
    seccion_container: {
        flexDirection: 'row'
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:'#EAEAEA',
        margin:10,
        alignSelf: 'center',
        width: '85%'
    },
    mercado: {
        "backgroundColor": "#F0F6F5",
        flexDirection: 'row',
        alignSelf: 'center',
        "borderTopLeftRadius": 10,
        "borderTopRightRadius": 10,
        "borderBottomRightRadius": 10,
        "borderBottomLeftRadius": 10,
        "fontFamily": "BRFirma-Bold",
        "fontSize": 16,
        "lineHeight": 19,
        "textAlign": "right",
        "color": "#FFFFFF",
        height: 30,
        marginHorizontal: 4,
        marginVertical: 10,
        paddingHorizontal: 12,
        paddingVertical: 5,
        alignItems: 'center'
    },
    mercado_texto: {
        padding: 10,
        fontFamily: 'BRFirma-Bold', 
    },
    precio: {
        fontSize: 20, 
        marginBottom:10, 
        marginLeft:'5%', 
        fontFamily: 'BRFirma-Bold', 
    },
})
