/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useContext } from 'react'

import { View, Text, TouchableOpacity,
      StyleSheet, ScrollView, Dimensions,
      ActivityIndicator, Linking } from 'react-native'
import { HorizontalSlider, sharing } from '../components'

import { Logo } from '../components'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import analytics from '@react-native-firebase/analytics';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const GOOGLE_PACKAGE_NAME = 'com.appvienebien'
const { width } = Dimensions.get('window');

export default ({ navigation }) => {
  const { platos } = useContext(PlatosContext)
  const [loading, setLoading] = useState(false);
  const [isVisbleModal, setVisibilty] = useState(false);

  useEffect(() => {}, [])

  const Share = () => (
    <View style={styles.modal_container}>
    <View style={styles.modal_line}/>
    <View style={{justifyContent: 'space-evenly', flex:1,}}>
        <TouchableOpacity
          accessibilityLabel="Comparte la App"
          onPress={()=>{
            sharing(); 
            analytics().logEvent('boton_compartir_app')
          }}
          style={{flexDirection: 'row', justifyContent: 'space-between'}}
        >
          <Text style={styles.modal_text}>Comparte la App</Text>
          <MaterialIcons name="keyboard-arrow-right" size={26} />
        </TouchableOpacity>
        <TouchableOpacity
          accessibilityLabel="Califícanos"
          onPress={()=> {
            analytics().logEvent('boton_calificanos')
            return Linking
              .openURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`)
          }}
          style={{flexDirection: 'row', justifyContent: 'space-between'}}
        >
          <Text style={styles.modal_text}>Califícanos</Text>
          <MaterialIcons name="keyboard-arrow-right" size={26} />
        </TouchableOpacity>
      </View>
    </View>
  )

  return (
    <>
    <ScrollView style={{backgroundColor: '#FFFFFF', flexGrow: 1, flex:1}}>
      <View style={{marginTop: 20, marginLeft: 30, marginRight:30}}>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Logo width={220} height={50} viewBox={'1 0 2.05 75'} fill={'#FF8A00'} />
          <TouchableOpacity onPress={()=>{ setVisibilty(true) }}>
            <Text style={{fontSize:24, color:'#FF8A00', fontWeight: 'bold', paddingHorizontal: 10}}>{'\u22EE'}</Text>
          </TouchableOpacity>
        </View>
        <Text style={{fontSize: 23, lineHeight: 30, fontFamily:'BRFirma-Bold', marginBottom: 20}}>
          Descubre los mejores platos para tu familia
        </Text>
      </View>
      <TouchableOpacity
        style={{
          alignItems: 'center', 
          justifyContent:'center',
          paddingLeft: 10,
          paddingRight: 10,
          flew: 1,
        }}
        onPress={() => {
          analytics().logEvent('abrir_busqueda')
          navigation.navigate(
            'Search',{
            goToLista: () => {navigation.navigate('Mi lista')},
          })
      }}
      >
        {/* Top (Search) */}
        <View style={styles.searchBar} elevation={4} >
          <Ionicons name={'search'} size={20} color={'black'} />
          <Text
            style={{
              color: '#999', 
              fontSize:14,
              paddingLeft: 10,
              fontFamily:"BRFirma-Regular"
            }}
          >¿Algún plato en especial?</Text>
        </View>
      </TouchableOpacity>
      <View 
        style={{
          padding: 0, 
          marginTop: 10,
        }}>
        {loading && <ActivityIndicator size="large" color="#FF8A00" />}
        {!loading && 
          platos.map((e, k) =>(
            <View key={k}>
              <Text style={styles.subtitle}>
                {e.filtro}
              </Text>
              <HorizontalSlider 
                navigate={navigation.navigate} 
                DATA={e.lista}
                filtroID={e.filtroId}
                filtro={e.filtro}
              />
              {/*k===0 && <Nutricionista/>*/}
            </View>
          ))}
      </View>
      <Modal 
        isVisible={isVisbleModal}
        style={{justifyContent: 'flex-end', margin: 0,}}
        onSwipeComplete={()=> setVisibilty(false)}
        swipeDirection='down'>
        <Share/>
      </Modal>
    </ScrollView>
    
    </>
    )
}

const styles = StyleSheet.create({
  searchBar:{
    alignItems: 'center', 
    justifyContent:'flex-start',
    padding: 12,
    flex: 1,
    flexDirection: 'row',
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 12
  },
  subtitle:{
    color: '#999999',
    fontSize: 16,
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 5,
    marginLeft: 20,
    textTransform: 'capitalize',
    fontFamily:'BRFirma-Bold'
  },
  selectionContainer:{
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  text:{
    backgroundColor: '#F0F6F5',
    color: '#777',
    borderRadius: 12,
    padding: 10,
    marginLeft: 10,
    marginBottom: 5,
    textTransform: 'capitalize',
  },
  selected:{
    backgroundColor: '#FF8A00',
    color: '#fff',
  },
  button:{
    backgroundColor: '#FF8A00',
    marginTop: 50,
    padding: 10,    
    alignItems: "center",
    borderRadius: 12, 
    justifyContent: 'flex-end',
    margin: 0,
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width: 1, height: 10},
    shadowRadius: 20,
    borderRadius: 10,
  },
  input: {
    flex: 1,
    fontSize: 18,
    paddingLeft: 8,
    alignSelf: 'stretch',
    minWidth: 0,
  },
  modal_container: {
    backgroundColor: 'white',
    padding: 18,
    height: 150,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12
  },
  modal_text:{
    fontSize: 16,
    marginHorizontal: 5,
    fontFamily:'BRFirma-Regular'
  },
  modal_line:{
    borderBottomColor: 'black',
    borderBottomWidth: 3,
    alignSelf: 'center',
    borderBottomColor: '#999',
    width: '50%',
    marginBottom: 10
  },
  contacto_container:{
    flexDirection: "row",
    backgroundColor: "#EEF6F5",
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius:12, 
    alignSelf: "center",
    justifyContent: "center", 
    alignItems: "center",
    width: width-40,
    marginHorizontal: 20,
    marginTop: 3,
    paddingRight: 40,
    paddingLeft: 60,
    paddingVertical: 0,
  },
  contacto_image: {
  }
});
