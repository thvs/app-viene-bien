/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useEffect, useContext, useState} from 'react'
import Dots from '../components/react-native-dots-pagination';
import Icon from "react-native-vector-icons/FontAwesome";
import { getData, storeData } from '../api/saveToLocal'

import { PlatosContext } from '../context'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { Logo, Button } from '../components'
import API from '../api'

let DATA = [
  {"id":1,"nombre":"desayuno"},
  {"id":2,"nombre":"almuerzo"},
  {"id":3,"nombre":"cena"},
  {"id":4,"nombre":"snack"},
  {"id":5,"nombre":"postre"}
]
/**
 * This shows the onboarding
 * @param  { Object } {navigation} - To change the current page
 */
export default ({ navigation }) => {
  const {updatePlatos} = useContext(PlatosContext)
  const [page, setPage] = useState(-1)

  const getRecetas = () => {
    const api = new API()
    // Fill the array with promises which initiate some async work
    const promises = DATA.map(e => api.getPlatos('', e.id, '','','',true));
    // Return a Promise.all promise of the array
    return Promise.all(promises);
  }

  useEffect(() => {
    let recetas = []
    getRecetas().then(res => { 
      res.forEach((res, k) => {
        //pasar lista de objetos a una lista normal
        recetas.push({
          filtro: DATA[k].nombre,
          filtroId: DATA[k].id,
          lista: Object.values(res.data)
        })
      })
      updatePlatos(recetas)

      getData('onboarding').then(value => {
        if(!value){
            storeData('onboarding', 'true');
            setPage(0);
        } else{
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home'}],
          })
        }
      }) 
    }) 
  }, []); 
      
  /**
   * This funcion
   */
  const Page = () => {
    const titulos = ['Encuentra más de 500 recetas peruanas', 'Crea tu lista de compras al instante', 'Compra los ingredientes en un clic']
    const sub = ['Y más de 250 recetas saludables', 'Obtén tu lista de compras automática, según lo que quieras cocinar en la semana.', 'Elige tus platos favoritos y obtén tu lista de compras automática.'];
    const link = [require('../assets/img/onboarding-0.png'), require('../assets/img/onboarding-1.png'), require('../assets/img/onboarding-2.png')]
    const size = [{w: 227, h:216}, {w: 225, h: 238}, {w: 216, h: 240}]
    return (
      <View
        style={{
          flex:1, 
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: page>=1?-50:-20,
          paddingLeft: 20,
          paddingRight: 20
        }}
      >
        {page>=1 &&
        <TouchableOpacity style={{padding: 5, width:'100%',}}>
          <View style={{
          borderRadius: 100,
          alignSelf: 'flex-start', 
          backgroundColor:"FF8A00"}}>
            <Icon 
              name="chevron-left"
              size={22}
              onPress={()=> setPage(page-1)}
              style={{padding: 10}}
              color="#000"/>
            </View>
          </TouchableOpacity>}
        <Image source={link[page]}
          style={{
              width: size[page].w,
              height: size[page].h,
              marginTop: page===0?32:0,
              marginBottom: 30,
        }}/>
        <Text
          style={{
            textAlign: 'center',
            fontFamily: 'BRFirma-Bold',
            marginBottom: 10,
            fontSize:22,
            color: '#111'
          }}>
            {titulos[page]}
          </Text>
          <Text style={{
                textAlign: 'center',
                fontSize:15,
                color: '#777',
                marginBottom: 10,
                paddingHorizontal: 40,
              }}
          >
            {sub[page]}
          </Text>
          <Dots length={3} active={page} 
            activeColor={'#FF8A00'} passiveColor = {'#FFCF9C'} 
            activeDotWidth={10} activeDotHeight={10}
            passiveDotWidth={10} passiveDotHeight={10}/>
          <View 
            style={{
              justifyContent:'flex-end', 
              marginTop: 50, 
              width: '100%'}}
          >
            <Button  
                text={page<2?"Siguiente":"¡A cocinar!"}
                type="primary" 
                onPress={()=> {
                  if (page >= 2){
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Home'}],
                    })
                  }
                  else{
                    setPage(page+1);
                  }
                }}
            />
          </View>        
      </View>
  )}


  return (<>
    {page===-1 && <View style={{flex: 1, alignItems: 'center',
      justifyContent: 'center' , height: 10}}>
      <Logo
        width={233}
        height={49}
        fill={'#777'}/>
    </View>}
    {page!=-1 && <Page/>}
    </>
  )
}