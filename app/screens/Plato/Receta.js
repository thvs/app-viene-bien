import React from 'react'
import Icon  from 'react-native-vector-icons/FontAwesome'
import { View, Text, StyleSheet } from 'react-native';
import FontAwesome5  from 'react-native-vector-icons/EvilIcons'

export default ({receta, tiempo, notify}) => {
  return (
    <View
    style={{
      marginHorizontal: 5,
      paddingTop: 15,
      flex: 1, 
    }}> 
      {notify && <View
      style={{
        marginHorizontal: 5, marginTop:5,
        marginBottom:16, paddingLeft: 12, paddingVertical:12,
        paddingRight: 20, borderRadius:12, flex: 1, 
        flexDirection:"row", justifyContent: 'space-between',
        paddingBottom: 16, backgroundColor: '#EEF6F5'
      }}>
        <Icon style={{marginTop: 2}} name="info-circle" color="#000" size={18}/>
        <View style={{marginLeft: 8}}>
          <Text style={{fontFamily: 'BRFirma-Bold', marginBottom:6, fontSize:15}}>
            ¡Lo sentimos! No pudimos actualizar las cantidades en la receta
          </Text>
          <Text style={{fontFamily: 'BRFirma-Regular'}}>
            Revisa las medidas correctas en la parte de ingredientes.
          </Text>
        </View>
      </View>}
      <View style={{
        flexDirection:"row", 
        justifyContent: 'space-between',
        paddingBottom: 20,
      }}>
          <Text
            style={{
              color:'#FF8A00',
              fontFamily: 'BRFirma-Bold'
            }}
          >Preparación</Text>
          <Text style={{fontFamily: 'BRFirma-Regular'}}>
            <FontAwesome5 name="clock" size={18} />
            {` ${tiempo}`}
          </Text>
      </View>
      <View style={styles.instructions_container}>
        <View style={styles.line_container}>
          <View style={styles.line}></View>
        </View>
        <View style={{flex:12}}>
          {receta.map((e, k) =>
            <View key={k} style={{flexDirection: 'row', marginBottom: 8 }}>
              <View>
                <Text style={{fontSize: 15, marginBottom: 3, fontFamily: 'BRFirma-Bold' }}>
                  {k+1}. {e.titulo}
                </Text>
                <View style={{flexDirection: 'row', width: '96%'}}>
                  <Icon style={{paddingRight: 5, marginTop: 6, paddingLeft:2}} 
                    name="circle" color="#777777" size={8}/>
                  <Text style={{ color:'#777', fontFamily: 'BRFirma-Regular'}}>
                    {e.descripcion}
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  instructions_container:{
    flexDirection:"row", 
    justifyContent: 'space-around',
    paddingBottom: 5,
    flex:1,
  },
  dots:{
    flex:1,
  },
  dot:{
    justifyContent: 'space-around',
    position: 'absolute',
    alignItems: 'stretch',
    left: '18%',
    backgroundColor:'white', height:16, width:16
  },
  line_container:{
    flexGrow: 1,
    justifyContent: 'center',
    borderStyle: 'dashed'
  },
  line: { 
    top: 0,
    bottom: 0,
    left: '45%',
    position: 'absolute',
    borderWidth: 0.5,
    height: '98%',
    alignItems: 'center',
    borderRadius: 0.00000001,
    borderColor: '#999',
  },  
  instructions:{

  }
})