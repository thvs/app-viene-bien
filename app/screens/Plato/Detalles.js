/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import ReadMore from 'react-native-read-more-text';
import analytics from '@react-native-firebase/analytics';

export default ({descripcion}) => {
  return(
    <View style={{paddingHorizontal:28, textAlign: 'center'}}>
      <View
        style={{
          padding: 5,
          paddingTop: 15,
          paddingBottom: 10,
          justifyContent: 'center',
          textAlign: 'center',
          alignItems: 'center',
        }}>
        <ReadMore 
          numberOfLines={4} 
          renderTruncatedFooter={renderTruncatedFooter} 
          renderRevealedFooter={renderRevealedFooter}>
          <Text style={styles.cardText}>{descripcion}</Text>
        </ReadMore>
      </View>
    </View>
  )
}

  

const renderTruncatedFooter = (handlePress) => {
  return (
    <Text
      style={{ color: '#FF8A00', marginTop: 5, fontSize: 12 }}
      onPress={()=>{handlePress(); analytics().logEvent('leer_mas', {receta: nombre})}}>
      Leer Mas
    </Text>
  );
};

const renderRevealedFooter = (handlePress) => {
  return (
    <Text
      style={{ color: '#FF8A00', marginTop: 5, fontSize: 12 }}
      onPress={handlePress}>
      Leer Menos
    </Text>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#ecf0f1',
  },
  cardText:{
    fontSize: 15,
    color: '#777',
    textAlign: 'center', 
    fontFamily:'BRFirma-Regular'
  },
});