/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react'
import { View, Text, Image, StyleSheet } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/MaterialIcons'


/*
  TODO: Verificar que la seleccion del producto sea correcta
  TODO: Validar el cambio de cantidad. 
  TODO: Agregar el estilo del texto.
*/
const url_ingredientes = 'https://www.database.vienebien.ludik.pe/api/storage/app/public/ingredientes'
export default ({ingredientes, width, update}) => {
  const [toggleCheckBoxUnico, setToggleUnico] = useState(()=>{
    const unico = ingredientes.perecibles;
    return unico.map(v => v.producto != null);
  })
  const [toggleCheckBoxMultiple, setToggleMultiple] = useState(()=>
    Array(ingredientes.no_perecibles? ingredientes.no_perecibles.length:0).fill(false)
  )

  useEffect(()=>{
      let array_1 = []
      for (let i = 0; i < ingredientes.perecibles.length; i++){
        const flag = ingredientes.perecibles[i].producto !== null
        array_1.push(flag)
      }
      
      let array_2 = []
      for (let i = 0; i < ingredientes.no_perecibles.length; i++)
        array_2.push(false)
  
      setToggleUnico(array_1)
      setToggleMultiple(array_2)
      update(toggleCheckBoxUnico, toggleCheckBoxMultiple)
  }, [])

  function updateToggle(uno, k){
    if (uno){
      let copy = toggleCheckBoxUnico;
      copy[k] = !toggleCheckBoxUnico[k]==undefined?true:!toggleCheckBoxUnico[k];
      setToggleUnico(copy)
    }
    else{
      let copy = toggleCheckBoxMultiple;
      copy[k] = !toggleCheckBoxMultiple[k]==undefined?true:!toggleCheckBoxMultiple[k];
      setToggleMultiple(copy)
    }
    update(toggleCheckBoxUnico, toggleCheckBoxMultiple)
  }

  return (
    <View style={style.container}>
      <View style={style.inner_container}>
      {ingredientes.perecibles.length>0 &&
        <Text style={style.ing_titulo}>Frescos</Text>}
      {ingredientes.perecibles.map((e, k) => {
        const precio = e.producto?.precioFinal || 0
        return (
          <View key={k} style={{...style.ing_container,width: width}}>
            <CheckBox
              disabled          = { !e.producto  }
              tintColors        = { { true: '#FF8A00', false: '#FF8A00' }}
              value             = { toggleCheckBoxUnico[k] }
              style             = {{opacity: !e.producto?0:1}}
              onPress           = { () => updateToggle(true, k)}
              onCheckBoxPressed = { () => updateToggle(true, k)}
              onValueChange     = { () => updateToggle(true, k)}
            />
            <View style={{ flexDirection:"row", alignItems: 'center', width: '70%'}} >
              <Image source = {{uri:`${url_ingredientes}/${e.imagen}`}}
                resizeMode="contain"
                style = {{ 
                  width: 75, 
                  height: 70,
                  marginRight: 10,
                }}
              />
                    
              <View style={{alignSelf: 'center', width: '80%'}}>
                <Text style={style.ing_name}>
                  {e.nombre}
                </Text>
                {precio!=0 && <Text style={{color: '#FF8A00', fontFamily: 'BRFirma-Regular'}}>
                    {`S/${precio}`}
                  </Text>}
                {!e.producto && 
                <Text style={style.sin_stock_text}>
                  <Icon 
                    name="block" 
                    color={"#EA4335"} 
                    size={14} 
                    style={{marginTop: 5, paddingTop: 5}} 
                  />
                  Sin Stock
                </Text>}
              </View>
            </View>
            <Text style={{
              justifyContent: 'space-evenly',
              color: '#777',
              marginRight: 5
            }}>
              {e.unidadReceta}
            </Text>
          </View>)
      })}
      
      {ingredientes.no_perecibles.length>0 &&
        <Text style={style.ing_titulo}>De almacén</Text>}
      {ingredientes.no_perecibles.map((e, k) => {
        const precio = e.producto?.precioFinal || 0

        return (
          <View key={k} style={{...style.ing_container,width: width}}>
            <CheckBox
              disabled={!e.producto }
              tintColors={{ true: '#FF8A00', false: '#FF8A00' }}
              value     = {toggleCheckBoxMultiple[k]}
              style     = {{opacity: !e.producto?0:1}}
              onPress   = {()=>updateToggle(false, k)}
              onCheckBoxPressed={()=>updateToggle(false, k)}
              onValueChange={ () =>  updateToggle(false, k)}
            />
            <View style={{ flexDirection:"row", alignItems: 'center', width: '70%'}} >
              <Image source = {{uri:`${url_ingredientes}/${e.imagen}`}}
                resizeMode="contain"
                style = {{ 
                  width: 75, 
                  height: 70,
                  marginRight: 10,
                }}
              />
              <View style={{alignSelf: 'center', width: '80%'}}>
                <Text style={style.ing_name}>
                  {e.nombre}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  {precio!=0 && <Text style={{color: '#FF8A00', fontFamily: 'BRFirma-Regular'}}>
                    {`S/${precio}`}
                  </Text>}
                  {!e.producto && 
                  <Text style={style.sin_stock_text}>
                    <Icon 
                      name="block" 
                      color={"#EA4335"} 
                      size={14} 
                      style={{marginTop: 5, paddingTop: 5}} 
                    />
                    Sin Stock
                  </Text>}
                </View>
              </View>
            </View>
              <Text style={{
                justifyContent: 'space-evenly',
                color: '#777',
                marginRight: 5
              }}>
              {e.unidadReceta}
            </Text>
          </View>)
    })}
      </View>
  </View>
  )
}

const style = StyleSheet.create(
  {
    container:{
      flex: 1,
      paddingTop: 10,
      padding: 5, 
      alignItems: 'center', 
      justifyContent: 'center'
    },
    inner_container:{
      padding: 5,
      paddingTop: 10,
      alignItems: 'center', 
      justifyContent: 'center', 
    },
    ing_container:{
      flexDirection:"row", 
      justifyContent: 'space-between',
      alignSelf: 'center',
      alignItems:"center",
      paddingLeft: 15,
      paddingRight:15,
      borderBottomWidth: 0.5,
      borderColor: '#EAEAEA',
    },
    ing_name:{
      fontFamily: 'BRFirma-Bold',
      fontSize: 16,
      textTransform: 'capitalize',
    },
    ing_titulo:{
      alignSelf:"flex-start",
      marginLeft: 20,
      fontSize:18,
      fontFamily: 'BRFirma-SemiBold',
      color: "#777"
    },
    sin_stock_text:{
      color:"#EA4335", 
      marginLeft: 10, 
      fontFamily: 'BRFirma-SemiBold',        
    }
  },
)
