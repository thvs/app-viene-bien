import React, {useEffect} from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { Card } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal';

export default ({numIng, setVisible, goToLista, total, showAll, goBack}) => {
  useEffect(()=>{
    let timer = setTimeout(()=>{
      if (!showAll) setVisible();
    }, 4000);

    return () => clearTimeout(timer);
  }, [])

  const renderContent = () => (
    <Card 
      style={{...styles.container, height: showAll?200:120}}
      onPress={()=>{if (showAll) setVisible();}}
    >
      <View style = {{flex:1}} >
        <Text style={{
            fontSize: 20,
            paddingBottom: 15,
            fontFamily: 'BRFirma-Regular',
            textAlign: 'center'
          }}
        >
         {`¡¡Agregaste ${numIng} ingredientes al `}
          <Icon name="shopping-cart" size={18} color="#FF8A00"/>
          !
        </Text>
        <Text
          style={{
            fontSize: 15, 
            paddingBottom: 18   ,
            color: '#999',
            textAlign: 'center',
            color: '#FF8A00',
            fontFamily: 'BRFirma-Regular'
          }}>
          {`Ya tienes ${total()} ${total()>1?'recetas agregadas':'receta agregada'}`}
        </Text>
        {showAll && <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
          <TouchableOpacity style={styles.sbutton} 
            accessibilityLabel="Seguir comprando"
            onPress={()=>{setVisible(); goBack()}}
          >
            <Text style={styles.buttonText}>
            Seguir comprando
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} 
            accessibilityLabel="Ver lista de compras X"
            onPress={()=>{
              setVisible(); storeData('@verlista', true); goToLista();
            }}
          >
            <Text style={styles.buttonText}>
              Ir al carrito
            </Text>
          </TouchableOpacity>
        </View>}
      </View>
    </Card>
  );

  const storeData = async (key, value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
      // saving error
    }
  }

  return (
      <Modal 
        isVisible={true}
        style={{justifyContent: 'flex-end', margin: 0}}
        onBackdropPress={setVisible}
        onSwipeComplete={setVisible}
        swipeDirection='down'>
        {renderContent()}
      </Modal>
  )
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 18,
    margin:0,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  close:{
    fontSize: 24
  },
  subtitle:{
    color: '#999',
    fontSize: 16,
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 5,
  },
  selectionContainer:{
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  text:{
    backgroundColor: '#F0F6F5',
    color: '#777',
    borderRadius: 12,
    padding: 10,
    marginLeft: 10,
    marginBottom: 5,
  },
  selected:{
    backgroundColor: '#FF8A00',
    color: '#fff',
  },
  button:{
    backgroundColor: '#FF8A00',
    padding: 5,    
    flexWrap:'wrap', 
    alignItems: "center",
    borderRadius: 12, 
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width:5, height: 10},
    shadowRadius: 20,
    elevation: 4, 
    justifyContent: 'center',
    width: '48%'
  },
  sbutton:{
    backgroundColor: '#ccc',
    padding: 10,    
    alignItems: "center",
    borderRadius: 12, 
    justifyContent: 'center',
    flexWrap:'wrap', 
    shadowColor: '#000',
    shadowOpacity: 0.12,
    shadowOffset: {width:5, height: 10},
    shadowRadius: 20,
    elevation: 4, 
    width: '48%'
  },
  buttonText:{
    color: '#fff',
    fontSize: 17,
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'BRFirma-Bold'
  }
});