/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useContext, useRef } from 'react'

import { default as Detalles } from './Detalles'
import { default as Ingredientes } from './Ingredientes'
import { default as Receta } from './Receta'
import { default as Agregar } from './Agregar'

import { Card, Button } from '../../components'
import { View, Text, StyleSheet, TouchableOpacity,
  Dimensions, ActivityIndicator, ImageBackground, Image } from 'react-native';

import { getData, storeData } from '../../api/saveToLocal'
import analytics from '@react-native-firebase/analytics';
import Icon from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal';
import { ListaContext } from '../../context'
import { inject, observer } from "mobx-react";

// We can use this to make the overlay fill the entire width
const { width } = Dimensions.get('window');
import API from '../../api'
import { ScrollView } from 'react-native-gesture-handler'
// TODO: UNIDAD REBANADA ENCONTRAR SHORT VERSION.
const Plato = ({ route, store, navigation : { goBack } }) => {
  const [nav, setNav] = useState(() => -1)
  const [load, setLoading] = useState(true)

  const [plato, setPlato] = useState(() => {})
  const [porcion, setPorcion] = useState(()=>{})
  const [precio, setPrecio] = useState(()=>{})
  const [x, setIngredientes] = useState(()=>{})
  const [listita, setListita] = useState(()=>({unico:[], multiple:[]}))

  const [ingSelected, setIng] = useState(() => 0)

  const [ isVisible, setVisible ] = React.useState(false)
  const { updateLista } = useContext(ListaContext);
  const { updateMenu, getTotalItems } = store
  const { id, nombre, goToLista, uri } = route.params;
  const firstUpdate = useRef(true);

  useEffect(() => {
      //loading - api call
      //add plate here
      getData('porcion').then (res => {
        let porcion = 5

        if (res == null){
          storeData('porcion', porcion);
        }
        else{
          porcion = res
        }

        setPorcion(porcion)
        const api = new API();
        api.getPlato(id, porcion)
          .then(res => {
            let newPlato = {}
            const tiempo_split = res.tiempoPreparacion.split(' ')
            const medida = tiempo_split[1]==='minutos'? ' m':' h'
            
            newPlato.descripcion = res.detalles
            newPlato.ingredientes = JSON.parse(JSON.stringify(res.ingredientes))
            newPlato.receta = res.pasos
            newPlato.tiempo = tiempo_split[0] + medida
            newPlato.porcion = porcion
  
            setIngredientes(newPlato.ingredientes)
  
            //calcular precio 
            let crrPrice = 0;
  
            if (res.ingredientes.no_perecibles.length > 1) {           
              crrPrice += res.ingredientes.no_perecibles
                .reduce((a, b) => {
                  const priceA = a.producto?.precioFinal || 0
                  const priceB = b.producto?.precioFinal || 0 

                  console.log(`${priceA}, ${priceB}, <irale`)
                  return {producto:{precioFinal: priceA + priceB}};
              }).producto.precioFinal;
            }
            else if (res.ingredientes.no_perecibles.length > 0){
              let ing = res.ingredientes.no_perecibles[0]
              
              crrPrice += ing.producto?.precioFinal || 0
            }
          
            setPrecio(crrPrice)
            console.log(`>>>> ${crrPrice}`)
            setPlato(newPlato)
            setNav(()=>0)
            setLoading(false)
            firstUpdate.current = false;
          })
          .catch(err => console.log(err))
      })
    }, [])

  useEffect(() => {
      //loading - api call
      //add plate here
      if (firstUpdate.current) {
        return;
      }

      setLoading(true)
      const api = new API();
      
      api.getPlato(id, porcion)
        .then(res => {
          const newPlato = plato
          
          newPlato.ingredientes = JSON.parse(JSON.stringify(res.ingredientes))
          newPlato.porcion = porcion

          setIngredientes(newPlato.ingredientes)

          //calcular precio 
          let crrPrice = 0;
          let listaIng = res.ingredientes.no_perecibles

          if(listaIng.length > 1) {           
            crrPrice += listaIng
              .reduce((a, b) => {
                const priceA = a.producto?.precioFinal || 0
                const priceB = b.producto?.precioFinal || 0
                console.log(`MatweebUNICO-- >> ${priceA} >> ${priceB}`)
      
                return {producto: {price: priceA + priceB}};
            }).producto.precioFinal;
          }
          else if (listaIng.length > 0){
            let ing = listaIng[0]
            
            crrPrice += ing.producto?.precioFinal || 0
          } 

          setPrecio(crrPrice)
          setPlato(newPlato)
          setLoading(false)
        })
    }, [porcion])
  
  /**
   * Actualizar lista de ingredientes
   * @param  {Array} ing    - Nueva lista de ingredientes
   * @param  {Array} lista  - Lista de ingredientes actual
   * @return Lista de ingredientes actualizada
   */
  const agregarIngredientes = (ing, lista) => {
    ing.map(v => {
      let index = lista.findIndex(e=> e.id === v.id)
      const gindx = v.equivalencias.findIndex(e => e.medidaAmigable === v.unidad)
      let g = gindx !== -1 ? v.equivalencias[gindx].gramos : 1 * v.cantidad;

      if (index===-1){
        const ingrediente = {
          'id': v.id,
          'nombre': v.nombre, 
          'uri': v.imagen, 
          'producto': null,
          'gramos': g,
          'unidad' : v.unidad,
          'precio' : v.costo_total,
          'userSelected': v.userSelected || false,
        }
        lista.push(ingrediente);
      }
      else{
        lista[index].gramos += g;
      }
    });


    return lista; 
  }

  //Agregar a lista
  const addToMenu = () => {
    setVisible(true);
    const { uri } =  route.params
    const key = `menu_semanal`;     

    getData(key).then( async res => {
      let menu = res!==null?res:[]

      const newPlato = {
        'nombre': nombre, 
        'image': uri, 
        'id': id,
        'precio': precio,
        'ingredientes': JSON.parse(JSON.stringify(listita))
      }

      menu.push(newPlato)
      updateMenu(menu)
      storeData(key, menu)

      let lista = {unico: [], multiple: []};
      for (let r of menu){
        let ing = r.ingredientes
        if (ing.unico.length)
          lista.unico = agregarIngredientes (
            JSON.parse (
              JSON.stringify(ing.unico)), 
              lista.unico
            );
        if (ing.multiple.length)
          lista.multiple = agregarIngredientes (
            JSON.parse (
              JSON.stringify(ing.multiple)), 
                lista.multiple
            );
      }

      const api = new API();
      let nextList = {unico: [], multiple: []};
      api.generarListadeCompra(lista.unico)
        .then(res => {
          nextList.unico = res;
          api.generarListadeCompra(lista.multiple)
            .then ( res => {
              nextList.multiple = res;

              updateLista(nextList)
              setLoading(false)
          })
        }
      );
    })

    analytics().logEvent('boton_anadir_a_menu', {receta: nombre})
  }

  function updatePorcion (add) {
    const newPorcion = add ? porcion+1:porcion-1
    if (newPorcion<=0 || newPorcion>=15) return;

    setPorcion(newPorcion); 
    storeData('porcion', newPorcion)
  }

  const setNewPrice = (lista = null) => {
    const list = lista || listita;
    let crrPrice = 0;

    if (lista.perecibles){
      list.unico = lista.perecibles.filter(e => {
        return listita.unico.some(el => el.id === e.id);
      });
    }
    if (lista.no_perecibles){
      list.multiple = lista.no_perecibles.filter(e => {
        return listita.multiple.some(el => el.id === e.id);  
      });
    }

    console.log(`TST === ${lista.unico.length} == ${lista.multiple.length}`)

    if(list.unico.length > 1) {           
      crrPrice += list.unico
        .reduce((a, b) => {
          const priceA = a.producto?.precioFinal || 0
          const priceB = b.producto?.precioFinal || 0
          console.log(`MatweebUNICO >> ${priceA} >> ${priceB}`)

          return {producto: {price: priceA + priceB}};
      }).producto.precioFinal;
    }
    else if (list.unico.length > 0){
      let ing = list.unico[0]
      
      crrPrice += ing.producto?.precioFinal || 0
    } 
    if (list.multiple.length > 1) {           
      crrPrice += list.multiple
        .reduce((a, b) => {
          const priceA = a.producto?.precioFinal || 0
          const priceB = b.producto?.precioFinal || 0

          console.log(`Matweeb >> ${priceA} >> ${priceB}`)

          return {producto: {price: priceA + priceB}};
      }).producto.precioFinal;
    }
    else if (list.multiple.length > 0){
      let ing = list.multiple[0]
      
      crrPrice += ing.producto?.precioFinal || 0
    } 

    console.log(`PRECIOSL >> ${crrPrice}`)
    setPrecio(crrPrice);
  }

  const updateIngSeleccionados = (ingredientesP, ingredientesNP) => {
    const P = ingredientesP.filter(v=>v).length   
    const NP = ingredientesNP.filter(v=>v).length

    const newList = {unico:[], multiple: []}
    if (x.perecibles){
      x.perecibles
        .map((v, k)=> {console.log(ingredientesP[k]); console.log(v.nombre); if (ingredientesP[k]) newList.unico.push(v)})
    }
    if (x.no_perecibles){
      x.no_perecibles
        .map((v, k)=> {console.log(ingredientesNP[k]); console.log(v.nombre);  if (ingredientesNP[k]) newList.multiple.push(v)})
    }

    setNewPrice(newList);
    setListita(newList)
    setIng(P+NP)
  }

  return (
    <>
    <ScrollView 
      style={{backgroundColor: '#fff'}}
      showsVerticalScrollIndicator={false}>
      <ImageBackground
          source={{ uri: uri }}
          style={{
            ...StyleSheet.absoluteFill,
            height: 225,
            width: '100%',
            position: 'relative', // because it's parent
            top: 0,
            left: 0,
            resizeMode: 'stretch',
          }}>
          <TouchableOpacity 
            onPress={goBack}
            style={{padding: 5, marginBottom: 160}}>
            <View style={{padding: 10}}>
              <Image 
                source={require('../../assets/img/goback.png')}
                style={{padding: 10}}
                color="#fff" />
            </View>
          </TouchableOpacity>
          <Text style = { styles.nombre_receta }>
            {nombre}
          </Text>
      </ImageBackground>
      <Card style={{flex: 1, marginBottom: 0, paddingBottom: 0, width: width}}>
        {/*<Opinion show={firstLaunch} setShow={setFirstLauch} />*/}
        {load && 
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size="large" color="#FF8A00" />
          </View>}
        {!load && <>
          <Detalles descripcion={plato.descripcion}/>
          <InfoNutricional 
            nombre={nombre}/>
          <PrecioPorcion
            porcion = {porcion}
            precio  = {precio}
            update  = {updatePorcion}
          />
          <Navegacion
            nav={nav}
            setNav={setNav}
            nombre={nombre}
          />
         </>}
        {!load && nav === 0 && 
          <Ingredientes 
            ingredientes={x}
            width={width}
            porcion={porcion}
            update={updateIngSeleccionados}
          />}
        {!load && nav === 1 && 
          <Receta 
            receta={plato.receta} 
            tiempo={plato.tiempo}
            notify={porcion !== plato.porcion}
          />}
      </Card>
    </ScrollView>
    <View style={{backgroundColor: "#fff", alignItems: 'center',  paddingTop: 5, paddingBottom: 15}}>
      {!load && <Text style={{fontSize:16, fontFamily:'BRFirma-Bold', marginVertical: 10}}>
        <Icon name="gift" size={16} style={{marginRight: 5}}/>
          Recibe S/20 en tu primera compra
        <TouchableOpacity
          style={{paddingHorizontal: 10 ,height: 16, width:16}}
          onPress={()=>{console.log("TEST")}}
        >
          <Icon name="question-circle" color="#FF8A00" size={16} />
        </TouchableOpacity>
      </Text>}
      {!load && <Button type="child" onPress = { addToMenu } > 
          <Text style={styles.buttonText}>
            {`(${ingSelected}) Añadir al carrito`} 
            <Icon name="shopping-cart" size={20}/>
          </Text>
        </Button>}
    </View>
    {isVisible && <Agregar
        showAll = {true}
        numIng = {ingSelected}
        setVisible={()=> setVisible(false)}
        total={getTotalItems}
        goToLista={goToLista}
        goBack={goBack}
      />}
    </>
  )
}

export default inject("store")(observer(Plato));

const SimpleModal = ({show, hide}) => (
  <Modal 
      isVisible={show}>
      <View style={styles.m_container}>
        <Text style={styles.m_title}>
        ¡Pronto tendrás esta opción disponible!
        </Text>
        <Button 
          text={'Aceptar'} 
          type='primary' 
          onPress={hide}
        />
      </View>
  </Modal>
) 

const InfoNutricional = ({nombre}) => {
  const [visible, setVisible] = React.useState(false)

  return <View style = {styles.nutricion_container}>
    <Image 
      source={require('../../assets/img/plato-sano.png')} 
      style={{
        marginVertical: -2,
        marginLeft:20, 
        height: 120,
        width: 120
      }}
    />
    <View>
      <Text 
        style={{
          textAlign: "center", 
          fontFamily:'BRFirma-Bold', 
          fontSize:16
        }}
      >
        Valores nutricionales
      </Text>
      <Text style={{
        textAlign: "center",  
        color:"#777", 
        fontSize: 14, 
        fontFamily:'BRFirma-Regular',
        marginVertical: 3, 
      }}>
        Mejora tu salud con esta información.
      </Text>
      <TouchableOpacity 
        onPress={()=>{
          setVisible(true);     
          //analytics().logEvent('boton_ver_valor_nutricional', {receta: nombre})
      }}>
        <Text 
        style={{
          textAlign: "center", 
          color:"#FF8A00", 
          textDecorationLine: 'underline', 
          fontFamily:'BRFirma-Bold'
        }}>
          Ver más
        </Text>
     </TouchableOpacity>
   </View>
   <SimpleModal
    show={visible}
    hide = {()=> setVisible(false)}
    />
  </View>
}

const PrecioPorcion = ({porcion, update, precio}) => ( 
  <View style={styles.inside_container}>
  <View>
    <Text style={styles.titlePrecio}>Precio referencial</Text>
      <Text style={styles.precio}>
        {`S/${precio}`}
      </Text>
  </View>
  <View style={styles.porcion}>
    <Text style={styles.texto_porcion}>
      Porciones
    </Text>
    <View style={{flexDirection: 'row'}}>
      <Icon 
        name="minus-circle" 
        color={porcion<=1 || porcion>14?"#999":"#FF8A00"}  
        size={24} style={{paddingRight:5, alignSelf:'center'}}
        onPress={() => update(false)}
      />
      <Text 
        style={{
          ...styles.texto_porcion, 
          ...styles.numero_porcion, 
          paddingHorizontal: 10  
        }}
      >
        {porcion}
      </Text>
      <Icon 
        name="plus-circle" 
        color={(porcion>=14)?"#999":"#FF8A00"} 
        size={24} 
        style={{paddingLeft:5, alignSelf:'center'}}
        onPress={()=>update(true)}
      />
    </View>
  </View>
</View>
)

const Navegacion = ({nav, setNav, nombre}) => {
    
  return(
    <View
      style={{
        width: width,
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'space-evenly',
      }}>
        <Text style={{
            color: nav===0?'#FF8A00':"#777",
              fontFamily: nav===0? "BRFirma-Bold":"BRFirma-Regular",
              fontSize: 14, 
              borderBottomColor: nav===0?'#FF8A00':"#777",
              alignSelf:"center", 
              borderBottomWidth:   nav===0? 2:1, 
          }} 
          onPress={() => { 
            setNav(0);
            analytics().logEvent('ver_ingredientes', {receta: nombre})
          }}>
          INGREDIENTES
        </Text>
      <Text style={{
          color: nav===1?'#FF8A00':"#777",
          fontFamily: nav==1? "BRFirma-Bold":"BRFirma-Regular",
          borderBottomColor: nav===1?'#FF8A00':"#777",
          alignSelf:"center", 
          borderBottomWidth:  nav===1? 2:1, 
        }} 
        onPress={() => {
          setNav(1)
          analytics().logEvent('ver_receta', {receta: nombre});
        }}>
        RECETA
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inside_container:{
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-evenly',
    marginBottom: 20, 
    marginHorizontal: 25,
  },
  nombre_receta: {  fontFamily: 'BRFirma-Bold', 
    backgroundColor:'rgba(0,0,0,0.1)', 
    paddingHorizontal:10,
    color: '#fff',
    padding: 0,
    position:'absolute',
    flexWrap: 'wrap',
    position: 'absolute', // child
    bottom: 15, // position where you want
    left: 25,
    fontSize: 22,
    width: '70%',
    marginBottom: 10,},
  titlePrecio:{
    color: '#000',
    fontSize: 14,
    paddingTop: 20,
    marginBottom: 5,
    fontFamily:'BRFirma-Regular'
  },
  precio: {
    fontSize: 21,
    fontFamily:'BRFirma-Bold',
    textAlign: "center"
  },
  porcion:{
    alignContent:'center',
    borderRadius: 12,
    padding: 10,
  },
  texto_porcion:{
    marginBottom: 5,
    color: 'black',
    alignSelf:'center',
    fontWeight: 'bold',
    fontFamily:'BRFirma-Regular'
  },
  numero_porcion:{
    fontSize: 20,
    fontWeight: 'normal',
    fontFamily:'BRFirma-Bold'
  },
  simbolos_porcion:{
    fontSize: 21,
    backgroundColor: '#FF8A00',
    padding: 20,
    color: 'black'
  },
  buttonText:{
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
    fontFamily:'BRFirma-Bold'
  },
  nutricion_container:{
    flexDirection: "row",
    backgroundColor: "#F7FEFD",
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius:12, 
    alignSelf: "center",
    justifyContent: "center", 
    alignItems: "center",
    width: width-40,
    marginHorizontal: 20,
    marginTop: 3,
    paddingRight: 80,
    paddingLeft: 60,
    paddingVertical: 5,
  },
  m_container: {
    backgroundColor: 'white',
    padding: 20,
    paddingHorizontal: 25,
    margin:0,
    borderRadius: 12,
    alignItems: 'center'
  },
  m_title: {
      marginBottom: 10,
      fontSize: 16,
      color: '#222',
      textAlign: 'center',
      fontFamily: 'BRFirma-Bold'
  },
});

