import React, {createContext, useState, useEffect} from 'react';
import { getData } from '../api/saveToLocal'

export default ListaContext = createContext();

/**
 * Estructura de lista:
 * {unico - multiple}
 * Ingrediente {
 *  id: Number,
 *  nombre: string,
 *  uri: string,
 *  producto: Producto
 *  cantidad: Number,
 *  unidad: string,
 *  gramos: Number, ? Maybe
 *  equivalencias: Array<any>
 * }
 *  Producto {
 *    id: Number,
 *    nombre: string,
 *    precio: Number, 
 *    uri: string,
 *    marca: string,
 *    cantidad: kg,
 *    en_kilos: boolean,
 *    userSelected: boolean,
 *  }
 */

export const ListaProvider = ({children}) => {
  const [lista, setLista] = useState(()=>{})

  useEffect(() => {
    const key = `listaDeCompra`

    getData(key)
      .then(res => setLista(res!== null?res:{}))
      .catch(err => setLista({}))
    }, [])

  const updateLista = lista => setLista(lista)

  const getTotal = vieneBien => {
    let t = 0;
    let multiplo = vieneBien ? 0.95 : 1;

    if (lista && 'unico' in lista)
      lista.unico.forEach(e=> {
        t += (e.producto ?
          e.producto.price * ( e.producto.cantidadkg && e.producto.unidadCantidad === 'gr' ?  (e.producto.cantidad > 50 ? e.producto.cantidad / 1000 : e.producto.cantidad): 1)
        : 0) * multiplo;
        })
    if (lista && 'multiple' in lista)
      lista.multiple.forEach(e=> t += 
        (e.producto ?
          e.producto.price * (e.producto.cantidadkg && e.producto.unidadCantidad === 'gr' ?  (e.producto.cantidad > 50 ? e.producto.cantidad / 1000 : e.producto.cantidad): 1)
        : 0) * multiplo)
    return t;
  }

  return (
    <ListaContext.Provider value={{lista, updateLista, getTotal}}>
      {children}
    </ListaContext.Provider>
  );
};