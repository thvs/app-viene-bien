// importing observables and decorate
import { action, makeAutoObservable } from "mobx";

class Store {
  constructor(){makeAutoObservable(this) }
  // observable to save image response from api
  menu = [];
  load = true

  // observables can be modifies by an action only
  @action updateMenu = menu => {
    this.menu = menu;
    this.toggleLoading(false)
  };

  @action getTotalItems = () => this.menu.length===0?1:this.menu.length;

  @action toggleLoading = load => this.load = load;
}

// export class
export default new Store();