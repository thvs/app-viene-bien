import React, {createContext, useState} from 'react';

export default PlatosContext = createContext();

export const PlatosProvider = ({children}) => {
  const [platos, setPlatos] = useState(()=>{})
  const [pantalla, setPantalla] = useState(()=>'Descubrir')

  const updatePlatos = platos => setPlatos(platos)

  return (
    <PlatosContext.Provider value={{platos, updatePlatos, pantalla, setPantalla}}>
      {children}
    </PlatosContext.Provider>
  );
};